﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace BaseApp.Common.Utils
{
    public class PathConverter
    {
        private string BaseDirectory;
        private static PathConverter InnerInstance;

        public static PathConverter InitInstance(string baseDirectory)
        {
            InnerInstance = new PathConverter(baseDirectory);
            return Current;
        }

        public static PathConverter Current
        {
            get
            {
                if(InnerInstance == null)
                    throw new Exception("You must call PathConverter.InitInstance befor use curent property.");
                return InnerInstance;
            }
        }

        private PathConverter(string baseDirectory)
        {
            if (String.IsNullOrWhiteSpace(baseDirectory))
                throw new ArgumentNullException("baseDirectory");
            BaseDirectory = baseDirectory;
        }

        public String EscapeInvalidCharsFromFileName(string p_FileName)
        {
            List<string> l_invalidChars = new List<string>();
            l_invalidChars.AddRange(Array.ConvertAll(Path.GetInvalidFileNameChars(), p_char => Regex.Escape(p_char.ToString())));

            string regex = "[" + string.Join("|", l_invalidChars.ToArray()) + "]";
            Regex removeInvalidChars = new Regex(regex, RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.CultureInvariant);

            return removeInvalidChars.Replace(p_FileName, "_");
        }

        public String FromAbsoluteToRelative(String p_sAbsolutePath)
        {
            return FromAbsoluteToRelative(BaseDirectory, p_sAbsolutePath);
        }

        public String FromAbsoluteToRelative(String p_sBaseDirectory, String p_sAbsolutePath)
        {
            if (String.Compare(p_sBaseDirectory, Path.GetDirectoryName(p_sAbsolutePath), true) == 0)
            {
                return Path.GetFileName(p_sAbsolutePath);
            }

            string[] arrFirstPathParts = p_sBaseDirectory.Trim(Path.DirectorySeparatorChar).Split(Path.DirectorySeparatorChar);
            string[] arrSecondPathParts = p_sAbsolutePath.Trim(Path.DirectorySeparatorChar).Split(Path.DirectorySeparatorChar);

            int iSamePartsCounter = 0;
            for (int i = 0; i < Math.Min(arrFirstPathParts.Length, arrSecondPathParts.Length); i++)
            {
                if (!arrFirstPathParts[i].ToLower().Equals(arrSecondPathParts[i].ToLower()))
                {
                    break;
                }
                iSamePartsCounter++;
            }

            if (iSamePartsCounter == 0)
            {
                return p_sAbsolutePath;
            }

            string sRelativePath = String.Empty;
            for (int i = iSamePartsCounter; i < arrFirstPathParts.Length; i++)
            {
                if (i > iSamePartsCounter)
                {
                    sRelativePath += Path.DirectorySeparatorChar;
                }
                sRelativePath += "..";
            }
            if (sRelativePath.Length == 0)
            {
                sRelativePath = ".";
            }
            for (int i = iSamePartsCounter; i < arrSecondPathParts.Length; i++)
            {
                sRelativePath += Path.DirectorySeparatorChar;
                sRelativePath += arrSecondPathParts[i];
            }
            return sRelativePath;
        }

        public String FromRelativeToAbsolute(String p_sRelativePath)
        {
            return FromRelativeToAbsolute(BaseDirectory, p_sRelativePath);
        }

        public String FromRelativeToAbsolute(String p_sBaseDirectory, String p_sRelativePath)
        {
            String sAbsolutePath = p_sRelativePath;
            if (!Path.IsPathRooted(p_sRelativePath))
            {
                sAbsolutePath = Path.GetFullPath(Path.Combine(p_sBaseDirectory, p_sRelativePath));
            }
            return sAbsolutePath;
        }
    }
}