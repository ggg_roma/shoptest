﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace BaseApp.Common.Utils
{
    public static class ImageHelper
    {
        public static Image ResizeImage(Image srcImage, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / srcImage.Width;
            var ratioY = (double)maxHeight / srcImage.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(srcImage.Width * ratio);
            var newHeight = (int)(srcImage.Height * ratio);

            Bitmap newImage = new Bitmap(newWidth, newHeight);
            using (Graphics gr = Graphics.FromImage(newImage))
            {
                gr.SmoothingMode = SmoothingMode.AntiAlias;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(srcImage, new Rectangle(0, 0, newWidth, newHeight));
            }

            return newImage;
        }
    }
}
