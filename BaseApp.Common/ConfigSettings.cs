﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using BaseApp.Common.Utils;

namespace BaseApp.Common
{
    public static class ConfigSettings
    {
        public static bool ShowFullError
        {
            get
            {
                return String.Equals("true", ConfigurationManager.AppSettings.Get("ShowFullError"),
                                     StringComparison.OrdinalIgnoreCase);
            }
        }

        public static string AttachmentsFolder
        {
            get
            {
                string path = ConfigurationManager.AppSettings.Get("AttachmentsFolder");
                return String.IsNullOrWhiteSpace(path) 
                    ? String.Empty 
                    : PathConverter.Current.FromRelativeToAbsolute(path);
            }
        }

        public static HashSet<string> AllowedEmailAddresses
        {
            get
            {
                HashSet<string> l_set = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                string l_addr = ConfigurationManager.AppSettings.Get("AllowedEmailAddresses");
                if (!String.IsNullOrEmpty(l_addr))
                {
                    string[] l_arr = l_addr.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(m => m.Trim())
                        .Where(m => !String.IsNullOrWhiteSpace(m)).ToArray();
                    l_set.UnionWith(l_arr);
                }
                return l_set;
            }
        }

        public static bool IsEmailAddressAllowed(String p_emailAddressList)
        {
            bool l_res = true;
            if (!String.IsNullOrWhiteSpace(p_emailAddressList))
            {
                HashSet<string> l_addressesAllowed = AllowedEmailAddresses;

                if (l_addressesAllowed.Count > 0)
                {
                    String[] l_addresses = p_emailAddressList.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string l_address in l_addresses)
                    {
                        if (!l_addressesAllowed.Contains(l_address))
                        {
                            l_res = false;
                            break;
                        }
                    }
                }
            }
            return l_res;
        }

        public static bool IsTraceEnabled
        {
            get
            {
                var section = ConfigurationManager.GetSection("system.web/trace") as ConfigurationSection;
                if (section != null)
                {
                    var prop = section.ElementInformation.Properties["enabled"];
                    if (prop != null)
                    {
                        return String.Equals("true", prop.Value.ToString(),
                                         StringComparison.OrdinalIgnoreCase);
                    }
                }
                return false;
            }
        }

        public static Enums.DbDataInitStrategyTypes DbDataInitStrategyType
        {
            get
            {
                var rawValue = ConfigurationManager.AppSettings.Get("DbDataInitStrategyType");
                if (string.IsNullOrWhiteSpace(rawValue))
                    return Enums.DbDataInitStrategyTypes.MigrateValidate;

                return (Enums.DbDataInitStrategyTypes)Enum.Parse(typeof(Enums.DbDataInitStrategyTypes), rawValue, true);
            }
        }
    }
}
