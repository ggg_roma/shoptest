﻿namespace BaseApp.Common
{
    public class Enums
    {
        public enum NotificationEmailTypes
        {
            ResetPassword = 1
        }

        public enum MenuItemTypes
        {
            
        }

        public enum DbDataInitStrategyTypes
        {
            MigrateValidate,
            MigrateToLatest,
            None
        }

        public enum SchedulerOnDateShiftTypes
        {
            //TODO: implement
        }
    }
}
