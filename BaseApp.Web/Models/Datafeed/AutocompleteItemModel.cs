﻿namespace BaseApp.Web.Models.Datafeed
{
    public class AutocompleteItemModel
    {
        public string Label { get; set; }
        public string Value { get; set; }
    }
}