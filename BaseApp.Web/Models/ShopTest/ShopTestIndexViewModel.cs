﻿using System.Collections.Generic;

namespace BaseApp.Web.Models.ShopTest
{
    public class ShopTestIndexViewModel
    {
        public IEnumerable<TestUserViewModel> Users { get; set; }
    }
}