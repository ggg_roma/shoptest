﻿namespace BaseApp.Web.Models.ShopTest
{
    public class TestUserViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}