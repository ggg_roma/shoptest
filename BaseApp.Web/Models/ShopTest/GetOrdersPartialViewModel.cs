﻿using System.Collections.Generic;

namespace BaseApp.Web.Models.ShopTest
{
    public class GetOrdersPartialViewModel
    {
        public List<OrderViewModel> Orders { get; set; }
    }
}