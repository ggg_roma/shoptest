﻿using BaseApp.Web.Code.Infrastructure;

namespace BaseApp.Web.Models.ShopTest
{
    public class GetGoodsInput
    {
        [NotDefaultValueRequired]
        public int OrderId { get; set; }
    }
}