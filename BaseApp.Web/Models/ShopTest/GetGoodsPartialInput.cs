﻿using BaseApp.Web.Code.Infrastructure;

namespace BaseApp.Web.Models.ShopTest
{
    public class GetGoodsPartialInput
    {
        [NotDefaultValueRequired]
        public int OrderId { get; set; }
    }
}