﻿using System.Collections.Generic;

namespace BaseApp.Web.Models.ShopTest
{
    public class GetGoodsPartialViewModel
    {
        public List<GoodViewModel> Goods { get; set; }
    }
}