﻿using BaseApp.Web.Code.Infrastructure;

namespace BaseApp.Web.Models.ShopTest
{
    public class GetOrdersPartialInput
    {
        [NotDefaultValueRequired]
        public int UserId { get; set; }
    }
}