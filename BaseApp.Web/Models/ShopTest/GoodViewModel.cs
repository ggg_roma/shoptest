﻿namespace BaseApp.Web.Models.ShopTest
{
    public class GoodViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}