﻿namespace BaseApp.Web.Models.Errors
{
    public class NotFoundViewModel
    {
        public string RequestedUrl { get; set; }
        public string ReferrerUrl { get; set; }
    }
}