﻿namespace BaseApp.Web.Models.Errors
{
    public class ErrorInfoModel 
    {
        public bool AllowShowMessage { get; set; }
        public string FormattedMessage { get; set; }
    }
}
