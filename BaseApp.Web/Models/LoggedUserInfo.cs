﻿using System;
using System.Linq;
using System.Security.Principal;
using BaseApp.Common;

namespace BaseApp.Web.Models
{
    public class LoggedUserInfo
    {
        public int UserID { get; private set; }
        public string Login { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string[] Roles { get; private set; }

        public string FullName
        {
            get { return String.Format("{0} {1}", FirstName, LastName).Trim(); }
        }

        public bool IsAdmin
        {
            get { return Roles.Contains(Constants.Roles.Admin); }
        }

        public LoggedUserInfo(int userId, string login, string firstName, string lastName, string[] roles)
        {
            UserID = userId;
            Login = login;
            FirstName = firstName;
            LastName = lastName;
            Roles = roles ?? new string[0];
        }
    }

    public class UserIdentity : IIdentity
    {
        public string AuthenticationType { get; private set; }
        public bool IsAuthenticated { get; private set; }
        public string Name { get; private set; }

        public UserIdentity(string authenticationType, bool isAuthenticated, string name)
        {
            AuthenticationType = authenticationType;
            IsAuthenticated = isAuthenticated;
            Name = name;
        }
    }

    public class UserPrincipal : GenericPrincipal
    {
        public LoggedUserInfo UserInfo { get; private set; }

        public UserPrincipal(IIdentity identity, LoggedUserInfo userInfo)
            : base(identity, userInfo.Roles)
        {
            UserInfo = userInfo;
        }
    }
}
