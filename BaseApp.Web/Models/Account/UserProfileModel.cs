﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BaseApp.Common.Extensions;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Code.Infrastructure;
using DataAnnotationsExtensions;

namespace BaseApp.Web.Models.Account
{
    public class UserProfileModel : ValidatableModelBase
    {
        [Required]
        [Display(Name = "Login")]
        public string Login { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required, Email]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        protected override IEnumerable<ValidationResult> Validate(UnitOfWork unitOfWork, LoggedUserInfo currentUser, ValidationContext validationContext)
        {
            if (!String.Equals(currentUser.Login, Login, StringComparison.OrdinalIgnoreCase))
            {
                var byLogin = unitOfWork.Users.GetByLoginOrNull(Login, true);
                if (byLogin != null && byLogin.Id != currentUser.UserID)
                {
                    yield return new ValidationResult("Login already in use.", new[] { this.GetPropertyName(m => m.Login) });
                }
            }
        }
    }
}