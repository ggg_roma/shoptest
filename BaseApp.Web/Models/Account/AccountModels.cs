﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using BaseApp.Common;
using BaseApp.Common.Extensions;
using BaseApp.Common.Utils;
using BaseApp.Data.DataContext.Projections.Users;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Code.Infrastructure;
using DataAnnotationsExtensions;

namespace BaseApp.Web.Models.Account
{

    public class ChangePasswordModel : ValidatableModelBase
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = Constants.MinPasswordLength)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        protected override IEnumerable<ValidationResult> Validate(UnitOfWork unitOfWork, LoggedUserInfo currentUser, ValidationContext validationContext)
        {
            var user = unitOfWork.Users.GetIncludeRolesOrNull(currentUser.UserID);
            if (!PasswordHash.ValidatePassword(OldPassword, user.Password))
            {
                yield return new ValidationResult("The current password is incorrect", new[] { this.GetPropertyName(m => m.OldPassword) });
            }
        }
    }

    public class LogOnModel : ValidatableModelBase
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }

        protected override IEnumerable<ValidationResult> Validate(UnitOfWork unitOfWork, LoggedUserInfo currentUser, ValidationContext validationContext)
        {
            var user = unitOfWork.Users.GetAccountByLoginOrNull(UserName);
            return ValidateUser(user, Password);
        }

        public static IEnumerable<ValidationResult> ValidateUser(AccountProjection user, string userPassword)
        {
            if (user == null || !PasswordHash.ValidatePassword(userPassword, user.Password))
            {
                yield return new ValidationResult("The user name or password provided is incorrect.");
            }
            else
            {
                if (!user.Roles.Any())
                    yield return new ValidationResult("User has no one role.");
            }
        }
    }

    public class RegisterModel : ValidatableModelBase
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
        
        [Required, Email]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        protected override IEnumerable<ValidationResult> Validate(UnitOfWork unitOfWork, LoggedUserInfo currentUser, ValidationContext validationContext)
        {
            if (unitOfWork.Users.GetByLoginOrNull(UserName, true) != null)
            {
                yield return new ValidationResult("User name already in use.", new[] { this.GetPropertyName(m => m.UserName) });
            }
            else if (unitOfWork.Users.GetByEmailOrNull(Email, true) != null)
            {
                yield return new ValidationResult("Email already in use.", new[] { this.GetPropertyName(m => m.Email) });
            }
        }
    }

    public class ForgotPasswordModel : ValidatableModelBase
    {
        [Email, Required]
        public string Email { get; set; }

        protected override IEnumerable<ValidationResult> Validate(UnitOfWork unitOfWork, LoggedUserInfo currentUser, ValidationContext validationContext)
        {
            var user = unitOfWork.Users.GetByEmailOrNull(Email);
            if (user == null)
            {
                yield return new ValidationResult("The email address could not be found.", new[] { this.GetPropertyName(m => m.Email) });
            }
        }
    }

    public class CompleteResetPasswordModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = Constants.MinPasswordLength)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
