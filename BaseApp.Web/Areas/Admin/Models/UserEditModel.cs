﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BaseApp.Common.Extensions;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Code.Infrastructure;
using BaseApp.Web.Models;
using DataAnnotationsExtensions;

namespace BaseApp.Web.Areas.Admin.Models
{
    public class UserEditModel : ValidatableModelBase
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required, Email]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "User Roles")]
        [Required]
        public List<UserRoleModel> Roles { get; set; }

        protected override IEnumerable<ValidationResult> Validate(UnitOfWork unitOfWork, LoggedUserInfo currentUser, ValidationContext validationContext)
        {
            if (Roles == null || Roles.All(m => !m.Checked))
            {
                yield return new ValidationResult("At least one role required.", new[] { this.GetPropertyName(m => m.Roles) });
            }
        }
    }

    public class UserRoleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Checked { get; set; }
    }
}