﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Web.Mvc;
using BaseApp.Data.DataContext.Entities;
using BaseApp.Web.Areas.Admin.Models;
using BaseApp.Web.Code.Extensions;
using BaseApp.Web.Code.Infrastructure.BaseControllers;
using AutoMapper;

namespace BaseApp.Web.Areas.Admin.Controllers
{
    public class UserController : ControllerBaseAdminRequired
    {
        public ActionResult Index()
        {
            return View(GetUserList());
        }

        public ActionResult Edit(int id)
        {
            var user = UnitOfWork.Users.GetOrNull(id);
            if (user == null)
                return NotFoundAction();

            UserEditModel model = Mapper.Map<UserEditModel>(user);

            model.Roles = Mapper.Map<List<UserRoleModel>>(UnitOfWork.Users.GetAllRoles());
            model.Roles.ForEach(m => m.Checked = user.Roles.Any(r => r.Id == m.Id));

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(UserEditModel model)
        {
            if(ModelState.IsValid)
            {
                var user = UnitOfWork.Users.Get(model.Id);

                user = Mapper.Map(model, user);
                user.UpdatedByUserId = User.GetUserInfo().UserID;
                user.UpdatedDate = DateTime.Now;

                user.Roles.Clear();

                List<Role> allRoles = UnitOfWork.Users.GetAllRoles();
                foreach (var role in model.Roles.Where(m => m.Checked))
                {
                    user.Roles.Add(allRoles.Single(m => m.Id == role.Id));                                
                }

                UnitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            User user = UnitOfWork.Users.Get(id);
            user.DeletedDate = DateTime.Now;
            user.DeletedByUserId = User.GetUserInfo().UserID;

            UnitOfWork.SaveChanges();

            if(Request.IsAjaxRequest())
            {
                return PartialView("_UserList", GetUserList());
            }
            return RedirectToAction("Index");
        }

        //TODO: PLEASE REMOVE THIS METHOD (ONLY FOR TRANSACTION EXAMPLE USED)
        [HttpPost]
        public ActionResult RestoreDeletedUsers()
        {
            Thread.Sleep(3000);

            List<User> users = UnitOfWork.Users.GetDeleted();

            using (var transaction = UnitOfWork.BeginTransaction())
            {
                foreach (var user in users)
                {
                    user.DeletedDate = null;
                    user.DeletedByUserId = null;
                    UnitOfWork.SaveChanges();
                }

                transaction.Commit();
            }

            if (Request.IsAjaxRequest())
            {
                return PartialView("_UserList", GetUserList());
            }
            return RedirectToAction("Index");   
        }

        private List<UserListItemModel> GetUserList()
        {
            List<User> users = UnitOfWork.Users.GetAll();
            return Mapper.Map<List<UserListItemModel>>(users);
        }
    }
}
