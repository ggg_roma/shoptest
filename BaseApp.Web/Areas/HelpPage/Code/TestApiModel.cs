﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseApp.Web.Areas.HelpPage.Code
{
    public class TestApiModel
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public FormMethod Method { get; set; }
        public string EncType { get; set; }
        public object Model { get; set; }
    }
}