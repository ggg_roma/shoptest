﻿using System;

namespace BaseApp.Web.Areas.HelpPage.Code
{
    public class ApiCustomTestAttribute : Attribute
    {
        public string Controller { get; set; }
        public string Action { get; set; }

        public ApiCustomTestAttribute(string controller, string action)
        {
            Controller = controller;
            Action = action;
        }
    }
}