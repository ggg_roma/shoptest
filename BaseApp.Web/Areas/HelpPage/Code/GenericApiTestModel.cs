﻿namespace BaseApp.Web.Areas.HelpPage.Code
{
    public class GenericApiTestModel
    {
        public string Url { get; set; }
        public string JsonBody { get; set; }
    }
}