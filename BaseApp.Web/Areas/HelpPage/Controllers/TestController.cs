﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using BaseApp.Web.Areas.HelpPage.Code;
using MultipartDataMediaFormatter.Infrastructure;

namespace BaseApp.Web.Areas.HelpPage.Controllers
{
    public class TestController : Controller
    {
        public TestController()
            : this(GlobalConfiguration.Configuration)
        {
        }

        public TestController(HttpConfiguration config)
        {
            Configuration = config;
        }

        public HttpConfiguration Configuration { get; private set; }

        public ActionResult ApiTest(string apiId)
        {
            if (String.IsNullOrEmpty(apiId))
            {
                throw new ArgumentNullException("apiId", "Not specified");
            }
            Collection<ApiDescription> apiDescriptions = Configuration.Services.GetApiExplorer().ApiDescriptions;
            ApiDescription apiDescription = apiDescriptions.FirstOrDefault(api => String.Equals(api.GetFriendlyId(), apiId, StringComparison.OrdinalIgnoreCase));

            if (apiDescription == null)
            {
                throw new Exception(string.Format("API with id {0} not found", apiId));
            }
            object model = null;

            if (apiDescription.ParameterDescriptions.Count > 1)
                throw new Exception("API test available only for 0 or 1 action parameters");

            var apiUnavail = apiDescription.ActionDescriptor.GetCustomAttributes<ApiUnavailableTestAttribute>().FirstOrDefault();
            if (apiUnavail != null)
            {
                return View("NoTestMethod");
            }

            var apiCustom = apiDescription.ActionDescriptor.GetCustomAttributes<ApiCustomTestAttribute>().FirstOrDefault();
            if (apiCustom != null)
            {
                return RedirectToAction(apiCustom.Action, apiCustom.Controller);
            }

            bool isMultiPart = false;
            if (apiDescription.ParameterDescriptions.Count == 1)
            {
                var firstParamType = apiDescription.ParameterDescriptions[0].ParameterDescriptor.ParameterType;
                if (firstParamType == typeof(FormData))
                    throw new Exception("API test unavailable for action parameter FormData");

                if (firstParamType.GetInterface(typeof(IMultipartData).Name) != null)
                {
                    isMultiPart = true;
                }

                if (firstParamType.IsClass && firstParamType != typeof(string))
                {
                    if (firstParamType.GetConstructor(Type.EmptyTypes) == null)
                    {
                        throw new Exception("API test unavailable for action parameter without empty constructor");
                    }

                    var objectGenerator = new ObjectGeneratorEx();
                    model = objectGenerator.GenerateObject(firstParamType);
                }
                else throw new Exception("API test available only for IsClass action parameter");
            }

            var testApiModel = new TestApiModel
            {
                Action = apiDescription.ActionDescriptor.ActionName,
                Controller = apiDescription.ActionDescriptor.ControllerDescriptor.ControllerName,
                Method = GetFormMethod(apiDescription.HttpMethod),
                Model = model
            };
            if (isMultiPart)
            {
                testApiModel.EncType = "multipart/form-data";
            }
            return View(testApiModel);
        }

        private static FormMethod GetFormMethod(HttpMethod apiMethod)
        {
            FormMethod method;
            if (apiMethod == System.Net.Http.HttpMethod.Get)
            {
                method = FormMethod.Get;
            }
            else if (apiMethod == System.Net.Http.HttpMethod.Post)
            {
                method = FormMethod.Post;
            }
            else throw new Exception(String.Format((string)"Test API for method {0} not supported", (object)apiMethod));
            return method;
        }
    }
}