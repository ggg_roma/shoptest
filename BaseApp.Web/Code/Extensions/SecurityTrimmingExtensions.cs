﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseApp.Web.Code.Extensions
{
    public static class SecurityTrimmingExtensions
    {
       /* public static bool HasActionPermission(this HtmlHelper htmlHelper, string actionName, string controllerName)
        {
            //if the controller name is empty the ASP.NET convention is:
            //"we are linking to a different controller
            ControllerBase controllerToLinkTo = string.IsNullOrEmpty(controllerName)
                                                    ? htmlHelper.ViewContext.Controller
                                                    : GetControllerByName(htmlHelper, controllerName);

            var controllerContext = new ControllerContext(htmlHelper.ViewContext.RequestContext, controllerToLinkTo);

            var controllerDescriptor = new ReflectedControllerDescriptor(controllerToLinkTo.GetType());

            var actionDescriptor = 
                controllerDescriptor.FindAction(controllerContext, actionName)
                ?? controllerDescriptor.GetCanonicalActions().FirstOrDefault(a => a.ActionName == actionName);

            return ActionIsAuthorized(controllerContext, actionDescriptor);
        }*/

        public static bool HasActionPermission(this UrlHelper urlHelper, ControllerBase controllerToLinkTo, string actionName)
        {
            var controllerContext = new ControllerContext(urlHelper.RequestContext, controllerToLinkTo);

            var controllerDescriptor = new ReflectedControllerDescriptor(controllerToLinkTo.GetType());

            var actionDescriptor =
                controllerDescriptor.FindAction(controllerContext, actionName)
                ?? controllerDescriptor.GetCanonicalActions().FirstOrDefault(a => a.ActionName == actionName);

            return ActionIsAuthorized(controllerContext, actionDescriptor);
        }

       /* public static bool ActionIsAccessibleToUser(this HtmlHelper htmlHelper, string actionName, string controllerName, string areaName)
        {
            // Determine controller type.
            var controllerResolver = new MvcSiteMapProvider.DefaultControllerTypeResolver();
            var controllerType = controllerResolver.ResolveControllerType(areaName, controllerName);
            if (controllerType == null)
            {
                throw new ArgumentException("Specified area or controller do not exist.");
            }

            // Get controller base.
            var controllerBase = (ControllerBase)Activator.CreateInstance(controllerType);

            return ActionIsAccessibleToUser(htmlHelper, actionName, controllerBase);
        }
*/
        private static bool ActionIsAuthorized(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            if (actionDescriptor == null)
                return false; // action does not exist so say yes - should we authorise this?!

            AuthorizationContext authContext = new AuthorizationContext(controllerContext, actionDescriptor);

            // run each auth filter until on fails
            // performance could be improved by some caching
            foreach (var filter in FilterProviders.Providers.GetFilters(controllerContext, actionDescriptor))
            {
                var authFilter = filter.Instance as IAuthorizationFilter;

                if (authFilter == null)
                    continue;

                authFilter.OnAuthorization(authContext);

                if (authContext.Result != null)
                    return false;
            }

            return true;
        }

       /* private static ControllerBase GetControllerByName(HtmlHelper helper, string controllerName)
        {
            // Instantiate the controller and call Execute
            IControllerFactory factory = ControllerBuilder.Current.GetControllerFactory();

            IController controller = factory.CreateController(helper.ViewContext.RequestContext, controllerName);

            if (controller == null)
            {
                throw new InvalidOperationException(

                    String.Format(
                        CultureInfo.CurrentUICulture,
                        "Controller factory {0} controller {1} returned null",
                        factory.GetType(),
                        controllerName));

            }

            return (ControllerBase)controller;
        }*/

    }
}