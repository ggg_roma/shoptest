﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseApp.Web.Code.Extensions
{
    public static class UrlHelperExtensions
    {
        public static string Home(this UrlHelper helper)
        {
            return helper.Action("Index", "Home", new { area = "" });
        }

        public static string AdminUsers(this UrlHelper helper)
        {
            return helper.Action("Index", "User", new { area = "admin" });
        }
    }
}