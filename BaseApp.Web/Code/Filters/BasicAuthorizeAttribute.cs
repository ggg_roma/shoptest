﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Models;
using BaseApp.Web.Models.Account;

namespace BaseApp.Web.Code.Filters
{
    public class BasicAuthorizeAttribute : AuthorizeAttribute
    {
        public bool RequireSsl { get; set; }
        public bool SendForbidden { get; set; }

        private void CacheValidateHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus)
        {
            validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null) 
                throw new ArgumentNullException("filterContext");

            if (AuthorizationDisabled(filterContext))
                return;

            if (!Authenticate(filterContext.HttpContext))
            {
                // HttpBasicUnauthorizedResult inherits from HttpUnauthorizedResult and does the
                // work of displaying the basic authentication prompt to the client
                filterContext.Result = new HttpBasicUnauthorizedResultMarker();
            }
            else
            {
                // AuthorizeCore is in the base class and does the work of checking if we have
                // specified users or roles when we use our attribute
                if (AuthorizeCore(filterContext.HttpContext))
                {
                    HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
                    cachePolicy.SetProxyMaxAge(new TimeSpan(0));
                    cachePolicy.AddValidationCallback(CacheValidateHandler, null /* data */);
                }
                else
                {
                    filterContext.Result = SendForbidden
                                               ? new HttpStatusCodeResult(HttpStatusCode.Forbidden)
                                               : (ActionResult) new HttpBasicUnauthorizedResultMarker();
                }
            }
        }

        // from here on are private methods to do the grunt work of parsing/verifying the credentials

        private static bool AuthorizationDisabled(AuthorizationContext filterContext)
        {
            //support new AllowAnonymousAttribute
            if (!filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
                return filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true);
            else
                return true;
        }

        private bool Authenticate(HttpContextBase context)
        {
            if (RequireSsl && !context.Request.IsSecureConnection && !context.Request.IsLocal) return false;

            string authHeader = context.Request.Headers["Authorization"];

            IPrincipal principal;
            if (TryGetPrincipal(authHeader, out principal))
            {
                HttpContext.Current.User = principal;
                return true;
            }
            return false;
        }

        private bool TryGetPrincipal(string authHeader, out IPrincipal principal)
        {
            var creds = ParseAuthHeader(authHeader);

            string userName = creds != null ? creds[0] : String.Empty;
            string userPassword = creds != null ? creds[1] : String.Empty;

            if (TryGetPrincipalInner(userName, userPassword, out principal)) 
                return true;

            principal = null;
            return false;
        }

        private string[] ParseAuthHeader(string authHeader)
        {
            // Check this is a Basic Auth header
            if (authHeader == null || authHeader.Length == 0 || !authHeader.StartsWith("Basic")) return null;

            // Pull out the Credentials with are seperated by ':' and Base64 encoded
            string base64Credentials = authHeader.Substring(6);
            string[] credentials = Encoding.ASCII.GetString(Convert.FromBase64String(base64Credentials)).Split(new char[] { ':' });

            if (credentials.Length != 2 || string.IsNullOrEmpty(credentials[0]) || string.IsNullOrEmpty(credentials[0])) return null;

            // Okay this is the credentials
            return credentials;
        }

        private bool TryGetPrincipalInner(string userName, string password, out IPrincipal principal)
        {
            var user = DependencyResolver.Current.GetService<IUnitOfWorkFactoryPerRequest>().UnitOfWork.Users.GetAccountByLoginOrNull(userName);
            var errors = LogOnModel.ValidateUser(user, password).ToList();

            if (errors.Count == 0)
            {
                var userInfo = new LoggedUserInfo(user.Id, user.Login, user.FirstName, user.LastName, user.Roles.ToArray());
                var userIdentity = new UserIdentity("basic", true, user.Login);
                principal = new UserPrincipal(userIdentity, userInfo);

                return true;
            }

            principal = null;
            return false;
        }
    }
    
    public class HttpBasicUnauthorizedResultMarker : ActionResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null) throw new ArgumentNullException("context");

            // See Application_EndRequest in Global.asax
            WebActionResultBase.SetRequestResult(new HttpBasicUnauthorizedResult(), context.HttpContext);
        }
    }

    public class HttpBasicUnauthorizedResult : WebActionResultBase
    {
        public override void ExecuteResult(HttpContextBase context)
        {
            if (context == null) throw new ArgumentNullException("context");

            context.Response.Clear();
            context.Response.StatusCode = 401;
            context.Response.StatusDescription = "Unauthorized - BaseApp.Web";
            context.Response.AddHeader("WWW-Authenticate", "Basic realm=\"BaseApp.Web\"");
            context.Response.Write("401 Unauthorized");
        }
    }
}