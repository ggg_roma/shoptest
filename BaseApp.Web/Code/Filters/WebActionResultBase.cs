﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseApp.Web.Code.Filters
{
    /// <summary>
    /// used to avoid WebForms iis module from redirect to login page
    /// </summary>
    public abstract class WebActionResultBase : ActionResult
    {
        private const string WebResultItemsKey = "WebResultItemsKey";

        public static void SetRequestResult(WebActionResultBase result, HttpContextBase context)
        {
            context.Items[WebResultItemsKey] = result;
        }

        public static WebActionResultBase GetRequestResult(HttpContextBase context)
        {
            return context.Items[WebResultItemsKey] as WebActionResultBase;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            ExecuteResult(context.HttpContext);
        }
        public abstract void ExecuteResult(HttpContextBase context);
    }
}