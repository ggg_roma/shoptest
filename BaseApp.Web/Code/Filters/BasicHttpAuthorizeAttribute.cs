﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Models;
using BaseApp.Web.Models.Account;

namespace BaseApp.Web.Code.Filters
{
    public class BasicHttpAuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {
        public bool SendForbidden { get; set; }

        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (filterContext == null) throw new ArgumentNullException("filterContext");

            if (AuthorizationDisabled(filterContext))
                return;

            if (!Authenticate(filterContext))
            {
                // HttpBasicUnauthorizedResult inherits from HttpUnauthorizedResult and does the
                // work of displaying the basic authentication prompt to the client
                filterContext.Response = new HttpBasicUnauthorizedResponseMessage();
            }
            else
            {
                // AuthorizeCore is in the base class and does the work of checking if we have
                // specified users or roles when we use our attribute
                base.OnAuthorization(filterContext);
                if (!base.IsAuthorized(filterContext))
                {
                    filterContext.Response =  SendForbidden
                        ? new HttpResponseMessage(HttpStatusCode.Forbidden) 
                        : new HttpBasicUnauthorizedResponseMessage();
                }
            }
        }

        // from here on are private methods to do the grunt work of parsing/verifying the credentials

        private static bool AuthorizationDisabled(HttpActionContext actionContext)
        {
            //support new AllowAnonymousAttribute
            if (!actionContext.ActionDescriptor.GetCustomAttributes<System.Web.Http.AllowAnonymousAttribute>().Any())
                return actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<System.Web.Http.AllowAnonymousAttribute>().Any();
            else
                return true;
        }

        private bool Authenticate(HttpActionContext actionContext)
        {
            var authHeader = actionContext.Request.Headers.Authorization;

            IPrincipal principal;
            if (authHeader != null && TryGetPrincipal(authHeader.Parameter, out principal))
            {
                if (HttpContext.Current != null)
                {
                    HttpContext.Current.User = principal;
                }
                Thread.CurrentPrincipal = principal;
                //for working httpRequestMessage.GetUserPrincipal()
                actionContext.Request.Properties.Add("MS_UserPrincipal", principal);
                return true;
            }
            return false;
        }

        private bool TryGetPrincipal(string authHeader, out IPrincipal principal)
        {
            var creds = ParseAuthHeader(authHeader);

            string userName = creds != null ? creds[0] : String.Empty;
            string userPassword = creds != null ? creds[1] : String.Empty;

            if (TryGetPrincipalInner(userName, userPassword, out principal))
                return true;

            principal = null;
            return false;
        }

        private string[] ParseAuthHeader(string authHeader)
        {
            // Check this is a Basic Auth header
            if (String.IsNullOrEmpty(authHeader))
                return null;

            // Pull out the Credentials with are seperated by ':' and Base64 encoded
            string[] credentials = Encoding.ASCII.GetString(Convert.FromBase64String(authHeader)).Split(new char[] { ':' });

            if (credentials.Length != 2 || String.IsNullOrEmpty(credentials[0]) || String.IsNullOrEmpty(credentials[0]))
                return null;

            // Okay this is the credentials
            return credentials;
        }

        private bool TryGetPrincipalInner(string userName, string password, out IPrincipal principal)
        {
            var user = DependencyResolver.Current.GetService<IUnitOfWorkFactoryPerRequest>().UnitOfWork.Users.GetAccountByLoginOrNull(userName);
            var errors = LogOnModel.ValidateUser(user, password).ToList();

            if (errors.Count == 0)
            {
                var userInfo = new LoggedUserInfo(user.Id, user.Login, user.FirstName, user.LastName, user.Roles.ToArray());
                var userIdentity = new UserIdentity("basic", true, user.Login);
                principal = new UserPrincipal(userIdentity, userInfo);

                return true;
            }

            principal = null;
            return false;
        }
    }

    public class HttpBasicUnauthorizedResponseMessage : HttpResponseMessage
    {
        public HttpBasicUnauthorizedResponseMessage()
            : base(HttpStatusCode.Unauthorized)
        {
            Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue("Basic", "realm=\"BaseApp.Web api\""));
        }
    }
}