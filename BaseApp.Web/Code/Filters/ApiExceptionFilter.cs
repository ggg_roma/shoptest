﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using BaseApp.Common;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Code.Formatters;
using BaseApp.Web.Code.Infrastructure;
using Newtonsoft.Json.Converters;

namespace BaseApp.Web.Code.Filters
{
    public class ApiExceptionFilter : ExceptionFilterAttribute 
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var errorCode = ApiResult.ApiResultCodes.Exception;
            if (context.Exception is ApiException)
            {
                var ex = context.Exception as ApiException;
                errorCode = ex.ErrorCode;
            }

            string errorMessageEx = String.Empty;
            if (context.Exception is System.Web.Http.HttpResponseException)
            {
                var ex = context.Exception as System.Web.Http.HttpResponseException;
                string content = ex.Response.Content != null 
                    ? ex.Response.Content.ReadAsStringAsync().Result
                    : "<null>";

                errorMessageEx = String.Format("Status code: {0} ({1})\r\nReason phrase: {2}\r\nContent: {3}",
                    ex.Response.StatusCode,
                    (int)ex.Response.StatusCode,
                    ex.Response.ReasonPhrase,
                    content);
            }

            if (errorCode != ApiResult.ApiResultCodes.FakeException)
            {
                var message = ("An unhandled exception has occurred.\r\n" + errorMessageEx).Trim();
                LogHolder.MainLog.ErrorException(message, context.Exception);
            }

            string errorMessage = (context.Exception.Message + "\r\n" + errorMessageEx).Trim();

            var statusCode = HttpStatusCode.InternalServerError;
            var apiResult = ApiResult.Error(errorCode, errorMessage);

            HttpResponseMessage responseMessage = context.Request.CreateResponse(statusCode, apiResult);

            if (apiResult != null && responseMessage.Content == null)
            {
                //no media type formatter found to serialize error details (need manually create response message)
                var formatter = new JsonpMediaTypeFormatter();
                formatter.SerializerSettings.Converters.Add(new StringEnumConverter());
                formatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
                responseMessage = new HttpResponseMessage(statusCode)
                                  {
                                      Content = new ObjectContent<ApiResult>(apiResult, formatter)
                                  };
            }

            context.Response = responseMessage;
        }
    }
}