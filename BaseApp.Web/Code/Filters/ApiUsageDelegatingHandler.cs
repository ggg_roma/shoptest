﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BaseApp.Web.Code.Infrastructure;

namespace BaseApp.Web.Code.Filters
{
    public class ApiUsageDelegatingHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var requestInfo = await GetRequestLoggingInfo(request);

            var response = await base.SendAsync(request, cancellationToken);

            var responseInfo = await GetResponseLoggingInfo(response);

            SaveLog(requestInfo, responseInfo);

            return response;
        }

        private async Task<ApiLoggingInfoRequest> GetRequestLoggingInfo(HttpRequestMessage request)
        {
            var info = new ApiLoggingInfoRequest();
            info.HttpMethod = request.Method.Method;
            info.UriAccessed = request.RequestUri.OriginalString;
            info.IpAddress = new IpAddressResolver(request).GetUserHostIp();
            info.Headers = ExtractMessageHeadersIntoLoggingInfo(request.Headers.ToList());

            if (request.Content != null)
            {
                var bytes = await request.Content.ReadAsByteArrayAsync();
                info.BodyContent = Encoding.UTF8.GetString(bytes);
            }

            return info;
        }

        private async Task<ApiLoggingInfoResponse> GetResponseLoggingInfo(HttpResponseMessage response)
        {
            var info = new ApiLoggingInfoResponse();
            info.ResponseStatusCode = response.StatusCode;
            info.ResponseStatusMessage = response.ReasonPhrase;
            info.Headers = ExtractMessageHeadersIntoLoggingInfo(response.Headers.ToList());

            if (response.Content != null)
            {
                var bytes = await response.Content.ReadAsByteArrayAsync();
                info.BodyContent = Encoding.UTF8.GetString(bytes);
            }

            return info;
        }

        private List<string> ExtractMessageHeadersIntoLoggingInfo(List<KeyValuePair<string, IEnumerable<string>>> headers)
        {
            var res = new List<string>();

            headers.ForEach(h =>
            {
                // convert the header values into one long string from a series of IEnumerable<string> values so it looks for like a HTTP header
                var headerValues = new StringBuilder();

                if (h.Value != null)
                {
                    foreach (var hv in h.Value)
                    {
                        if (headerValues.Length > 0)
                        {
                            headerValues.Append(", ");
                        }
                        headerValues.Append(hv);
                    }
                }
                res.Add(string.Format("{0}: {1}", h.Key, headerValues.ToString()));
            });

            return res;
        }

        private void SaveLog(ApiLoggingInfoRequest requestInfo, ApiLoggingInfoResponse responseInfo)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("--------------------------");
            builder.AppendLine("REQUEST:");

            builder.AppendLine("Uri: " + requestInfo.HttpMethod + " " + requestInfo.UriAccessed);
            builder.AppendLine("IP Address: " + requestInfo.IpAddress);

            builder.AppendLine("   Headers:");
            foreach (var header in requestInfo.Headers)
            {
                builder.AppendLine("      " + header);    
            }

            builder.AppendLine("Request body:");
            builder.AppendLine(requestInfo.BodyContent);

            builder.AppendLine("--------------------------");

            builder.AppendLine("RESPONSE:");

            builder.AppendLine("ResponseStatusCode: " + responseInfo.ResponseStatusCode);
            builder.AppendLine("ResponseStatusMessage: " + responseInfo.ResponseStatusMessage);

            builder.AppendLine("   Headers:");
            foreach (var header in responseInfo.Headers)
            {
                builder.AppendLine("      " + header);
            }

            builder.AppendLine("Response body:");
            builder.AppendLine(responseInfo.BodyContent);

            builder.AppendLine("--------------------------");

            System.Diagnostics.Trace.Write(builder.ToString(), "WEB API USAGE");
        }
    }

    public class ApiLoggingInfoRequest
    {
        public string HttpMethod { get; set; }
        public string UriAccessed { get; set; }
        public string IpAddress { get; set; }
        public string BodyContent { get; set; }
        public List<string> Headers { get; set; }

        public ApiLoggingInfoRequest()
        {
            Headers = new List<string>();
        }
    }

    public class ApiLoggingInfoResponse
    {
        public string BodyContent { get; set; }
        public HttpStatusCode ResponseStatusCode { get; set; }
        public string ResponseStatusMessage { get; set; }
        public List<string> Headers { get; set; }

        public ApiLoggingInfoResponse()
        {
            Headers = new List<string>();
        }
    }
}