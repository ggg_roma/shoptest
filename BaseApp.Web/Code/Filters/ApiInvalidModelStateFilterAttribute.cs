﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using BaseApp.Web.Code.Infrastructure;

namespace BaseApp.Web.Code.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class ApiInvalidModelStateFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                var errorsList = new List<string>();

                foreach (var field in actionContext.ModelState)
                {
                    if (field.Value.Errors.Count > 0)
                    {
                        string[] propNames = field.Key.Split('.');
                        if (propNames.Length > 1)
                        {
                            var actionParameteres = actionContext.ActionDescriptor.GetParameters().Select(m => m.ParameterName);
                            if (actionParameteres.Any(m => String.Equals(propNames[0], m, StringComparison.OrdinalIgnoreCase)))
                            {
                                //skip action method parameter name
                                propNames = propNames.Skip(1).ToArray();
                            }
                        }

                        string fName = string.Join(".", propNames);
                        if (!String.IsNullOrWhiteSpace(fName))
                        {
                            fName += ": ";
                        }

                        errorsList.AddRange(field.Value.Errors
                            .Select(m => fName + (NullIfEmpty(m.ErrorMessage) ?? (m.Exception != null ? m.Exception.Message : ""))));

                        for (int i = 0; i < errorsList.Count; i++)
                        {
                            var str = errorsList[i].Trim();
                            if (!String.IsNullOrWhiteSpace(str) && !str.EndsWith(".") && !str.EndsWith(";"))
                            {
                                errorsList[i] = str + ".";
                            }
                        }
                    }
                }

                throw new ApiException(ApiResult.ApiResultCodes.ArgumentValidationFailed, String.Join(" ", errorsList));
            }
        }

        private string NullIfEmpty(string value)
        {
            if (String.IsNullOrWhiteSpace(value))
                return null;
            return value;
        }
    }
}