﻿using BaseApp.Data.DataContext.Entities.TestShop;
using BaseApp.Web.Models.ShopTest;

namespace BaseApp.Web.Code.Mappers
{
    public class ShopTestMapper : MapperBase
    {
        protected override void CreateMaps()
        {
            CreateMap<TestUser, TestUserViewModel>();

            CreateMap<Good, GoodViewModel>();

            CreateMap<Order, OrderViewModel>();
        }
    }
}