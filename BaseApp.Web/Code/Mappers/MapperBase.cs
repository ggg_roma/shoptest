﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseApp.Web.Code.Mappers
{
    public abstract class MapperBase : AutoMapper.Profile
    {
        protected override void Configure()
        {
            base.Configure();
            CreateMaps();
        }

        protected abstract void CreateMaps();
    }
}