﻿using System;
using BaseApp.Data.DataContext.Entities;
using BaseApp.Web.Code.Extensions;
using System.Linq;
using BaseApp.Web.Models.Account;

namespace BaseApp.Web.Code.Mappers
{
    public class UserMapper : MapperBase
    {
        protected override void CreateMaps()
        {
            CreateMap<Role, Areas.Admin.Models.UserRoleModel>()
                .Ignore(m => m.Checked);

            CreateMap<Areas.Admin.Models.UserRoleModel, Role>()
                .Ignore(m => m.Users);


            CreateMap<User, Areas.Admin.Models.UserListItemModel>()
                .Map(m => m.Roles, d => String.Join(", ", d.Roles.Select(c => c.Name)));


            CreateMap<User, Areas.Admin.Models.UserEditModel>()
                .Ignore(m => m.Roles);

            CreateMap<Areas.Admin.Models.UserEditModel, User>()
                 .IgnoreAll() //Prefer .IgnoreAllUnmappedComplexTypes() method when it's possible
                 .Map(m => m.FirstName, t => t.FirstName)
                 .Map(m => m.LastName, t => t.LastName)
                 .Map(m => m.Email, t => t.Email)
                 .Map(m => m.Id, t => t.Id)
                 .Map(m => m.Roles, t => t.Roles);


            CreateMap<User, UserProfileModel>();

            CreateMap<UserProfileModel, User>()
               .IgnoreAll() //Prefer .IgnoreAllUnmappedComplexTypes() method when it's possible
               .Map(m => m.Login, t => t.Login)
               .Map(m => m.FirstName, t => t.FirstName)
               .Map(m => m.LastName, t => t.LastName)
               .Map(m => m.Email, t => t.Email);
        }
    }
}