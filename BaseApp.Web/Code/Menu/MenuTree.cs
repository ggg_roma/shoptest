﻿using System.Collections.Generic;

namespace BaseApp.Web.Code.Menu
{
    public class MenuTree
    {
        public List<MenuItem> Items { get; set; }

        public MenuTree()
        {
            Items = new List<MenuItem>();
        }
    }
}