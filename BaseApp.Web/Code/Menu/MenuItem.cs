﻿using System.Collections.Generic;
using System.Linq;
using BaseApp.Common;
using UrlHelper = System.Web.Mvc.UrlHelper;

namespace BaseApp.Web.Code.Menu
{
    public class MenuItem
    {
        public string Label { get; set; }
        public IUrlInfo UrlInfo { get; set; }
        public Enums.MenuItemTypes? MenuItemType { get; set; }
        public List<MenuItem> Items { get; set; }
        public string CssClass { get; set; }
        public bool OpenInNewWindow { get; set; }

        public MenuItem(string label)
        {
            Label = label;
            Items = new List<MenuItem>();
        }

        public bool HasItems
        {
            get { return Items != null && Items.Any(); }
        }

        public bool IsCurrent(UrlHelper urlHelper, Enums.MenuItemTypes? currentMenuItem)
        {
            if (currentMenuItem != null)
                return MenuItemType == currentMenuItem;

            return UrlInfo != null && UrlInfo.IsCurrent(urlHelper);
        }

        public bool HasPermission(UrlHelper urlHelper)
        {
            return UrlInfo.HasPermission(urlHelper);
        }

        public bool HasCurrentChild(UrlHelper urlHelper, Enums.MenuItemTypes? currentMenuItem)
        {
            return HasCurrentChild(Items, urlHelper, currentMenuItem);
        }

        private bool HasCurrentChild(List<MenuItem> items, UrlHelper urlHelper, Enums.MenuItemTypes? currentMenuItem)
        {
            foreach (var menuItem in items)
            {
                if (menuItem.IsCurrent(urlHelper, currentMenuItem) || HasCurrentChild(menuItem.Items, urlHelper, currentMenuItem))
                    return true;
            }
            return false;
        }
    }
}