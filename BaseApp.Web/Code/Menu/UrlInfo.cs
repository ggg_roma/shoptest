﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;
using BaseApp.Web.Code.Extensions;

namespace BaseApp.Web.Code.Menu
{
    public interface IUrlInfo
    {
        string Url { get; }
        bool HasPermission(UrlHelper urlHelper);
        bool IsCurrent(UrlHelper urlHelper);
    }

    public class UrlInfoRaw : IUrlInfo
    {
        public string Url { get; private set; }

        public UrlInfoRaw(string url)
        {
            Url = url;
        }

        public bool HasPermission(UrlHelper urlHelper)
        {
            return true;
        }

        public bool IsCurrent(UrlHelper urlHelper)
        {
            return false;
        }
    }

    public class UrlInfo : IUrlInfo
    {
        private string Controller { get; set; }
        private string Action { get; set; }
        private string Area { get; set; }
        private object RouteValues { get; set; }
        private Type ControllerType { get; set; }

        public string Url { get; private set; }

        protected UrlInfo(string url, Type controllerType, string action, string controller, string area = null, object routeValues = null)
        {
            Area = area ?? "";
            Action = action;
            RouteValues = routeValues;
            ControllerType = controllerType;
            Controller = controller;
            Url = url;
        }

        public static UrlInfo Create<T>(UrlHelper urlHelper, System.Linq.Expressions.Expression<Action<T>> action, string area, object routeValues)
            where T : ControllerBase
        {
            Type controllerType = typeof(T);

            string actionName = GetActionName(action);
            string controllerName = GetControllerName(controllerType);
            string url = MakeUrl(urlHelper, actionName, controllerName, routeValues, area);

            return new UrlInfo(url, controllerType, actionName, controllerName, area, routeValues);
        }

        public static UrlInfo Create<T>(UrlHelper url, System.Linq.Expressions.Expression<Action<T>> action, object routeValues = null)
            where T : ControllerBase
        {
            return Create(url, action, "", routeValues);
        }

        public static UrlInfo CreateAdmin<T>(UrlHelper url, System.Linq.Expressions.Expression<Action<T>> action, object routeValues = null)
            where T : ControllerBase
        {
            return Create(url, action, "Admin", routeValues);
        }

        public bool HasPermission(UrlHelper urlHelper)
        {
            var controller = (ControllerBase)DependencyResolver.Current.GetService(ControllerType);
            return urlHelper.HasActionPermission(controller, Action);
        }

        public bool IsCurrent(UrlHelper urlHelper)
        {
            RouteData routeData = urlHelper.RequestContext.RouteData;
            NameValueCollection queryString = urlHelper.RequestContext.HttpContext.Request.QueryString;

            var currentAction = (string)routeData.Values["action"];
            var currentController = (string)routeData.Values["controller"];
            var currentArea = (string)routeData.DataTokens["area"] ?? "";

            bool isCurrent = String.Equals(currentAction, Action, StringComparison.OrdinalIgnoreCase)
                   && String.Equals(currentController, Controller, StringComparison.OrdinalIgnoreCase)
                   && String.Equals(currentArea, Area, StringComparison.OrdinalIgnoreCase);

            if (isCurrent && RouteValues != null)
            {
                var r = new RouteValueDictionary(RouteValues);
                isCurrent = r.All(m =>
                {
                    object value = null;
                    if (routeData.Values.ContainsKey(m.Key))
                    {
                        value = routeData.Values[m.Key];
                    }
                    else if (queryString.AllKeys.Any(t => String.Equals(t, m.Key, StringComparison.OrdinalIgnoreCase)))
                    {
                        value = queryString[m.Key];
                    }

                    if (value != null)
                    {
                        var converter = TypeDescriptor.GetConverter(m.Value.GetType());
                        if (converter.CanConvertFrom(value.GetType()))
                        {
                            object convertedValue = converter.ConvertFrom(value);
                            return convertedValue!= null && convertedValue.Equals(m.Value);
                        }
                    }
                    return false;
                });
            }

            return isCurrent;
        }

        #region static helper methods

        private static string MakeUrl(UrlHelper urlHelper, string action, string controller, object routeValues,
            string area)
        {
            return urlHelper.Action(action, controller, MergeRouteValuesWithArea(routeValues, area));
        }

        private static RouteValueDictionary MergeRouteValuesWithArea(object routeValues, string area)
        {
            if (routeValues == null)
            {
                routeValues = new {};
            }

            var r = new RouteValueDictionary(routeValues);
            if (!r.Keys.Contains("area"))
            {
                r.Add("area", area);
            }
            return r;
        }

        private static string GetActionName<T>(Expression<Action<T>> expr) where T : IController
        {
            var memberExpr = expr.Body as MethodCallExpression;
            if (memberExpr == null)
                throw new ArgumentException("expr should represent access to a method");
            if (!typeof(ActionResult).IsAssignableFrom(memberExpr.Method.ReturnType))
                throw new ArgumentException("expr should represent access to a method that returns ActionResult");

            return memberExpr.Method.Name;
        }

        private static string GetControllerName(Type controllerType)
        {
            string controllerTypeName = controllerType.Name;
            string controller = "";
            string suffix = "Controller";
            if (controllerTypeName.EndsWith(suffix, StringComparison.OrdinalIgnoreCase))
            {
                controller = controllerTypeName.Remove(controllerTypeName.Length - suffix.Length, suffix.Length);
            }
            return controller;
        }

        #endregion
    }
}