﻿using System.Collections.Generic;
using System.Web.Mvc;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Code.Menu;
using BaseApp.Web.Controllers;
using BaseApp.Web.Models;

namespace BaseApp.Web.Code.MenuBuilders
{
    public class SiteMenuBuilder : MenuBuilderBase
    {
        public SiteMenuBuilder(UrlHelper urlHelper, LoggedUserInfo loggedUserInfo, UnitOfWork unitOfWork = null) : base(urlHelper, loggedUserInfo, unitOfWork)
        {
        }

        protected override List<MenuItem> GetMenuItems()
        {
            var menuItems = new List<MenuItem>();
            menuItems.Add(GetHomeMenuItem());
            menuItems.Add(GetGoogleMenuItem());
            menuItems.Add(GetProfileMenuItem());

            return menuItems;
        }
      
        private MenuItem GetHomeMenuItem()
        {
            return new MenuItem("Home")
            {
                UrlInfo = UrlInfo.Create<HomeController>(UrlHelper, m => m.Index())
            };
        }

        private MenuItem GetProfileMenuItem()
        {
            return new MenuItem("Profile")
            {
                UrlInfo = UrlInfo.Create<AccountController>(UrlHelper, m => m.UserProfile())
            };
        }

        private MenuItem GetGoogleMenuItem()
        {
            return new MenuItem("Search")
            {
                UrlInfo = new UrlInfoRaw("http://google.com"),
                OpenInNewWindow = true
            };
        }
    }
}