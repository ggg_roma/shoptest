﻿using System.Collections.Generic;
using System.Web.Mvc;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Areas.Admin.Controllers;
using BaseApp.Web.Code.Menu;
using BaseApp.Web.Models;

namespace BaseApp.Web.Code.MenuBuilders
{
    public class AdminMenuBuilder : MenuBuilderBase
    {
        public AdminMenuBuilder(UrlHelper urlHelper, LoggedUserInfo loggedUserInfo, UnitOfWork unitOfWork = null) : base(urlHelper, loggedUserInfo, unitOfWork)
        {
        }

        protected override List<MenuItem> GetMenuItems()
        {
            var items = new List<MenuItem>();
            items.Add(GetUsersMenuItem());

            return items;
        }

        private MenuItem GetUsersMenuItem()
        {
            return new MenuItem("Users")
            {
                UrlInfo = UrlInfo.CreateAdmin<UserController>(UrlHelper, m => m.Index())
            };
        }
    }
}