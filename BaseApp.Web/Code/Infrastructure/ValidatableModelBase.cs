﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Models;

namespace BaseApp.Web.Code.Infrastructure
{
    public abstract class ValidatableModelBase : IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return Validate(
                DependencyResolver.Current.GetService<IUnitOfWorkFactoryPerRequest>().UnitOfWork, 
                DependencyResolver.Current.GetService<LoggedUserInfo>(), 
                validationContext);
        }

        protected abstract IEnumerable<ValidationResult> Validate(UnitOfWork unitOfWork, LoggedUserInfo currentUser, ValidationContext validationContext);
    }
}