﻿using System.Web;

namespace BaseApp.Web.Code.Infrastructure.Templating
{
    public class TemplateBuilder 
    {
        public static string Render(string templateName, object model, object routeValues = null, HttpContext httpContext = null)
        {
            return new TemplateController().Render(templateName, model, routeValues, httpContext);
        }

    }
}