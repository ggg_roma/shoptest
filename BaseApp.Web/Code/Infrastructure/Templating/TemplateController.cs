﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;

namespace BaseApp.Web.Code.Infrastructure.Templating
{
    public class TemplateController : ControllerBase
    {
        public string Render(string templateName, object model, object routeValues = null, HttpContext httpContext = null)
        {
            StringBuilder builder = new StringBuilder();
            using (TextWriter sw = new StringWriter(builder))
            {
                /*var routeData = new RouteData();
                routeData.Values["controller"] = "Templates"; //template's folder name
                routeData.Values["action"] = templateName;

                ControllerContext controllerContext = new ControllerContext(new CustomHttpContext(), routeData, this);

                var viewResult = ViewEngines.Engines.FindView(controllerContext, templateName, null);
                ViewContext viewContext = new ViewContext(controllerContext, viewResult.View, new ViewDataDictionary(model), new TempDataDictionary(), sw);
                viewResult.View.Render(viewContext, sw);*/

                var viewUrl = templateName.StartsWith("~")
                    ? templateName
                    : "~/Views/Templates/" + templateName + ".cshtml";
                var viewPath = HostingEnvironment.MapPath(viewUrl);
                if (!File.Exists(viewPath))
                {
                    throw new FileNotFoundException("Template file not found - " + viewPath);
                }

                var customFakeHttpContext = httpContext != null
                    ? new HttpContextWrapper(httpContext)
                    : (HttpContextBase)(new CustomFakeHttpContext());

                /*
                 * HttpContext.Current != null 
                    ? new HttpContextWrapper(HttpContext.Current)
                    : (HttpContextBase)(new CustomFakeHttpContext());
                 */

                var routeData = new RouteData();

                if (routeValues != null)
                {
                    var routeDict = new RouteValueDictionary(routeValues);
                    foreach (var item in routeDict.ToList())
                    {
                        routeData.Values.Add(item.Key, item.Value);
                    }
                }

                /*var page = WebPageBase.CreateInstanceFromVirtualPath(viewUrl);
                 var httpContext = customFakeHttpContext;
                 var pageContext = new WebPageContext(httpContext, page, model);

                 page.ExecutePageHierarchy(pageContext, sw);*/

                var controllerContext = new ControllerContext(customFakeHttpContext, routeData, this);
                var view = new RazorView(controllerContext, viewUrl, null, false, null);
                var viewContext = new ViewContext(controllerContext, view, new ViewDataDictionary(model), new TempDataDictionary(), sw);
                view.Render(viewContext, sw);

                return builder.ToString();
            }
        }

        protected override void ExecuteCore()
        {
        }
    }

    public class CustomFakeHttpContext : HttpContextBase
    {
        private Hashtable innerItems = new Hashtable();
        private HttpRequestBase request = new CustomFakeHttpRequest();
        private HttpResponseBase response = new CustomFakeHttpResponce();
        private HttpSessionStateBase session = new CustomFakeHttpSessionState();
        private IPrincipal principal = new CustomFakePrincipal();
        private object pageInstrumentationService;

        public CustomFakeHttpContext()
        {
            //fix .net 4.5 issue (PageInstrumentationService is not implemented - not exist in .net 4.0)
            var name = typeof(System.Web.HttpContext).Assembly.FullName;
            var type = Type.GetType("System.Web.Instrumentation.PageInstrumentationService," + name, false);
            if (type != null)
            {
                pageInstrumentationService = Activator.CreateInstance(type);
            }
        }


        public override IDictionary Items
        {
            get { return innerItems; }
        }

        public override HttpSessionStateBase Session
        {
            get { return session; }
        }

        public override HttpRequestBase Request
        {
            get { return request; }
        }

        public override HttpResponseBase Response
        {
            get { return response; }
        }

        public override IPrincipal User
        {
            get { return principal; }
            set { principal = value; }
        }

        new public object PageInstrumentation
        {
            get { return pageInstrumentationService; }
        }

        public override System.Web.Caching.Cache Cache
        {
            get { return HttpRuntime.Cache; }
        }
    }

    public class CustomFakeHttpRequest : HttpRequestBase
    {
        private HttpCookieCollection cookies = new HttpCookieCollection();
        private HttpBrowserCapabilitiesBase httpBrowserCapabilities = new CustomFakeHttpBrowserCapabilities();
        private NameValueCollection serverVariables = new NameValueCollection();

        public override HttpCookieCollection Cookies
        {
            get { return cookies; }
        }

        public override bool IsLocal
        {
            get { return true; }
        }

        public override bool IsAuthenticated
        {
            get { return false; }
        }

        public override string UserAgent
        {
            get { return ""; }
        }

        public override HttpBrowserCapabilitiesBase Browser
        {
            get { return httpBrowserCapabilities; }
        }

        public override NameValueCollection ServerVariables
        {
            get { return serverVariables; }
        }
    }

    public class CustomFakeHttpBrowserCapabilities : HttpBrowserCapabilitiesBase
    {
        public override bool IsMobileDevice
        {
            get { return false; }
        }
    }

    public class CustomFakeHttpResponce : HttpResponseBase
    {
        private HttpCookieCollection cookies = new HttpCookieCollection();

        public override HttpCookieCollection Cookies
        {
            get { return cookies; }
        }
    }

    public class CustomFakeHttpSessionState : HttpSessionStateBase
    {
        private Dictionary<string, object> dictionary = new Dictionary<string, object>();

        public override void Add(string name, object value)
        {
            dictionary[name] = value;
        }

        public override void Remove(string name)
        {
            dictionary.Remove(name);
        }

        public override object this[string name]
        {
            get
            {
                object result;
                if (!dictionary.TryGetValue(name, out result))
                {
                    result = null;
                }
                return result;
            }
            set
            {
                dictionary[name] = value;
            }
        }
    }

    public class CustomFakePrincipal : IPrincipal
    {
        private IIdentity identity = new CustomFakeIdentity();

        public bool IsInRole(string role)
        {
            return false;
        }

        public IIdentity Identity
        {
            get { return identity; }
        }
    }

    public class CustomFakeIdentity : IIdentity
    {
        public string Name
        {
            get { return ""; }
        }

        public string AuthenticationType
        {
            get { return ""; }
        }

        public bool IsAuthenticated
        {
            get { return false; }
        }
    }
}