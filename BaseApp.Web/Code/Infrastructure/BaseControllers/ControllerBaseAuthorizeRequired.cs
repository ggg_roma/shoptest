﻿using System.Web.Mvc;

namespace BaseApp.Web.Code.Infrastructure.BaseControllers
{
    [Authorize]
    public abstract class ControllerBaseAuthorizeRequired : ControllerBaseNoAuthorize
    {
        
    }
}