﻿using System.Web.Mvc;
using BaseApp.Common;

namespace BaseApp.Web.Code.Infrastructure.BaseControllers
{
    [Authorize(Roles = Constants.Roles.Admin)]
    public abstract class ControllerBaseAdminRequired : ControllerBaseNoAuthorize
    {
        
    }
}