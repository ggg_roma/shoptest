﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Protocols;
using BaseApp.Data.Infrastructure;

namespace BaseApp.Web.Code.Infrastructure.BaseControllers
{
    public abstract class ControllerBaseNoAuthorize : System.Web.Mvc.Controller
    {
        private UnitOfWork _UnitOfWork;
        protected UnitOfWork UnitOfWork
        {
            get
            {
                if (_UnitOfWork == null)
                {
                    _UnitOfWork = DependencyResolver.Current.GetService<IUnitOfWorkFactoryPerRequest>().UnitOfWork;
                }
                return _UnitOfWork;
            }
        }

        /// <summary>
        /// client messages holder. ClientMessage.Render(ViewContext) call required in MasterPage 
        /// (or in current view  if ajax update used)
        /// </summary>
        public ScriptMessage ClientMessage { get; private set; }

        protected ControllerBaseNoAuthorize()
        {
            ClientMessage = new ScriptMessage();
        }

        public ActionResult NotFoundAction(string description = null)
        {
            throw new HttpException(404, description ?? "HTTP/1.1 404 Not Found");
        }


        /// <summary>
        /// Called before the action result that is returned by an action method is executed.
        /// </summary>
        /// <param name="filterContext">Information about the current request and action result</param>
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            ClientMessage.SaveMessages(filterContext.HttpContext);    
            base.OnResultExecuting(filterContext);
        }

        protected JsonResult JsonEx(object data = null, bool success = true)
        {
            return Json(new
                {
                    success = success,
                    data = data
                }, JsonRequestBehavior.AllowGet);
        }

        protected ContentResult CloseDialog(CloseDialogArgs args = null)
        {
            Response.Headers.Add("CloseDialog", "1");

            args = args ?? CloseDialogArgs.GetDefault();

            if (!string.IsNullOrWhiteSpace(args.ReturnResult))
            {
                Response.Headers.Add("CloseDialogResult", args.ReturnResult);
            }
            if (!args.DoNotShowSuccessMessage)
            {
                var msg = args.SuccessMessage;
                if (string.IsNullOrWhiteSpace(msg))
                    msg = "Saved successfully";

                ClientMessage.AddSuccess(msg);
            }
            
            return Content("");
        }

        public class CloseDialogArgs
        {
            public string SuccessMessage { get; set; }
            public string ReturnResult { get; set; }
            public bool DoNotShowSuccessMessage { get; set; }

            public static CloseDialogArgs GetDefault()
            {
                return new CloseDialogArgs();
            }
        }
    }
}