﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace BaseApp.Web.Code.Infrastructure
{
    public class ScriptMessage
    {
        public enum ScriptMessageTypes
        {
            Info,
            Error,
            Success,

            JsScript
        }

        private readonly List<ScriptMessageItem> Messages;
     
        public ScriptMessage()
        {
            Messages = new List<ScriptMessageItem>();
        }

        public void AddJavaScript(string script)
        {
            AddMessage(ScriptMessageTypes.JsScript, script);
        }
        public void AddError(string message)
        {
            AddMessage(ScriptMessageTypes.Error, message);
        }
        public void AddInfo(string message)
        {
            AddMessage(ScriptMessageTypes.Info, message);
        }
        public void AddSuccess(string message)
        {
            AddMessage(ScriptMessageTypes.Success, message);
        }

        private void AddMessage(ScriptMessageTypes type, string message)
        {
            Messages.Add(new ScriptMessageItem(type, message));
        }

        public void SaveMessages(HttpContextBase httpContext)
        {
            if(Messages.Count > 0)
            {
                string jsonMessages = JsonConvert.SerializeObject(Messages);
                string base64JsonMessage = Convert.ToBase64String(Encoding.UTF8.GetBytes(jsonMessages), Base64FormattingOptions.None);

                var cookie = new HttpCookie("SiteScriptMessage", base64JsonMessage);
                cookie.Path = "/";
                httpContext.Response.AppendCookie(cookie);
            }
        }
    }

    public class ScriptMessageItem
    {
        public ScriptMessage.ScriptMessageTypes MessageType { get; private set; }
        public String Message { get; private set; }

        public string MessageTypeString
        {
            get { return MessageType.ToString(); }
        }

        public string MessageDataType
        {
            get
            {
                switch (MessageType)
                {
                    case ScriptMessage.ScriptMessageTypes.Error:
                    case ScriptMessage.ScriptMessageTypes.Info:
                    case ScriptMessage.ScriptMessageTypes.Success:
                        return "messsage";
                    case ScriptMessage.ScriptMessageTypes.JsScript:
                        return "script";
                    default:
                        throw new ArgumentOutOfRangeException("Unknown messageType - "+ MessageType);
                }
            }
        }

        public ScriptMessageItem(ScriptMessage.ScriptMessageTypes messageType, string message)
        {
            MessageType = messageType;
            Message = message;
        }
    }
}