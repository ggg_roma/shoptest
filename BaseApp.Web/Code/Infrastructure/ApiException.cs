﻿using System;

namespace BaseApp.Web.Code.Infrastructure
{
    public class ApiException : Exception
    {
        public ApiResult.ApiResultCodes ErrorCode { get;  private set; }

        public ApiException(string message, Exception innerException = null)
            : this(ApiResult.ApiResultCodes.Exception, message, innerException)
        {
        }

        public ApiException(ApiResult.ApiResultCodes errorCode, string message = null, Exception innerException = null) 
            : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        public override string ToString()
        {
            return String.Format("Api error code: {0}{1}{2}", ErrorCode, Environment.NewLine, base.ToString());
        }
    }
}