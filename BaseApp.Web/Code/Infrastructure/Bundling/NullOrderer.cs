﻿using System.Collections.Generic;
using System.Web.Optimization;

namespace BaseApp.Web.Code.Infrastructure.Bundling
{
    /// <summary>
    /// used for avoid file ordering by Bundle (order as added from BundleConfig)
    /// </summary>
    public class NullOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}