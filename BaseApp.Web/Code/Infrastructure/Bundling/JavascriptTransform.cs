﻿using System;
using System.IO;
using System.Text;
using System.Web.Hosting;
using System.Web.Optimization;
using Yahoo.Yui.Compressor;

namespace BaseApp.Web.Code.Infrastructure.Bundling
{
    public class JavascriptTransformOptions
    {
        public bool EnableMinification { get; set; }
    }

    public class JavascriptTransform : IBundleTransform
    {
        private readonly JavascriptTransformOptions options;

        public JavascriptTransform(JavascriptTransformOptions options)
        {
            if (options == null)
                throw new ArgumentNullException("options");
            this.options = options;
        }

        public void Process(BundleContext context, BundleResponse response)
        {
            var compressor = new JavaScriptCompressor();
            var content = new StringBuilder();
            foreach (var f in response.Files)
            {

                string fileContent = File.ReadAllText(HostingEnvironment.MapPath(f.VirtualFile.VirtualPath));
                if (options.EnableMinification && !f.VirtualFile.Name.EndsWith(".min.js", StringComparison.OrdinalIgnoreCase))
                {
                    fileContent = compressor.Compress(fileContent);
                }
                content.Append(fileContent);
                content.AppendLine(";"); // We need to be sure that every js file ends with semicolon
            }
            
            response.Content = content.ToString();
            response.ContentType = "text/javascript";
        }
    }
}