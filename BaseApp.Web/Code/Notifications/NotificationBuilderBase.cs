﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseApp.Web.Code.Notifications
{
    public abstract class NotificationBuilderBase
    {
        protected NotificationBuilderBase(int notificationBuilderId)
        {
            
        }

        public void CreateNotification()
        {
            
        }

        public void ScheduleNotification()
        {

        }
    }
}