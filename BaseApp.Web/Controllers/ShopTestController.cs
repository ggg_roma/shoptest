﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using BaseApp.Web.Code.Infrastructure.BaseControllers;
using BaseApp.Web.Models.ShopTest;

namespace BaseApp.Web.Controllers
{
    public class ShopTestController : ControllerBaseNoAuthorize
    {
        public ActionResult Index()
        {
            var users = Mapper.Map<List<TestUserViewModel>>(UnitOfWork.TestUsers.GetAll());
            var model = new ShopTestIndexViewModel
            {
                Users = users
            };
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> GetOrdersPartial(GetOrdersPartialInput input)
        {
            var orders = Mapper.Map<List<OrderViewModel>> (await UnitOfWork.Orders.GetOrdersByUserIdAsync(input.UserId));
            var model = new GetOrdersPartialViewModel
            {
                Orders = orders
            };
            return PartialView("_GetOrdersPartial", model);
        }

        [HttpGet]
        public async Task<ActionResult> GetGoodsPartial(GetGoodsPartialInput input)
        {
            var goods = Mapper.Map<List<GoodViewModel>> (await UnitOfWork.OrderGoods.GetGoodsByOrderIdAsync(input.OrderId));
            var model = new GetGoodsPartialViewModel
            {
                Goods = goods
            };
            return PartialView("_GetGoodsPartial", model);
        }
    }
}