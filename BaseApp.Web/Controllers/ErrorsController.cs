﻿using BaseApp.Common;
using BaseApp.Common.Extensions;
using BaseApp.Data.Infrastructure;
using BaseApp.Web.Code.Infrastructure;
using BaseApp.Web.Code.Infrastructure.BaseControllers;
using BaseApp.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BaseApp.Web.Code.Extensions;
using BaseApp.Web.Models.Errors;

namespace BaseApp.Web.Controllers
{
    public class ErrorsController : ControllerBaseNoAuthorize
    {
        private IpAddressResolver _ipAddressResolver;

        public ErrorsController(HttpContextBase context)
            : base()
        {
            _ipAddressResolver = new IpAddressResolver(context);
        }

        public ActionResult General(Exception exception)
        {
            if (exception == null)
            {
                exception = (Exception)TempData["exception"];
            }

            if (exception is HttpException
                && (exception as HttpException).GetHttpCode() == (int)HttpStatusCode.NotFound)
            {
                return NotFound();
            }

            bool showFullMessage = ConfigSettings.ShowFullError
                    || Request.IsLocal
                    || (User.GetUserInfo() != null && User.GetUserInfo().IsAdmin);

            Response.StatusCode = 500;
            Response.TrySkipIisCustomErrors = true;

            if (Request.IsAjaxRequest())
            {
                string errorText = showFullMessage && exception != null
                    ? exception.GetBaseException().Message
                    : "Server error occured.";
                return Content(errorText);
            }
            else
            {
                ErrorInfoModel info = new ErrorInfoModel();
                if (showFullMessage)
                {
                    info.AllowShowMessage = true;
                    info.FormattedMessage = GetErrorMessage(exception);
                }
                return View("Error", info);
            }
        }

        public ActionResult NotFound()
        {
            Response.StatusCode = (int)HttpStatusCode.NotFound;
            Response.TrySkipIisCustomErrors = true;

            if (Request.IsAjaxRequest())
            {
                return Content("Resource not found: " + GetRequestedUrl(Request));
            }
            else
            {
                var model = new NotFoundViewModel
                    {
                        RequestedUrl = GetRequestedUrl(Request),
                        ReferrerUrl = GetReferrerUrl(Request, Request.Url.OriginalString)
                    };
                return View("NotFound", model);
            }
        }

        private String GetErrorMessage(Exception exception)
        {
            string sEventMessage = "";

            string userLogin;
            try
            {
                userLogin = HttpContext.User.Identity.Name;
            }
            catch
            {
                userLogin = "N/A";
            }

            string hostIP = null;
            string hostName = null;
            try
            {
                hostIP = _ipAddressResolver.GetUserHostIp(); //context.Request.ServerVariables["REMOTE_ADDR"];
                hostName = System.Net.Dns.GetHostEntry(hostIP).HostName;
            }
            catch
            {
                if (String.IsNullOrEmpty(hostIP))
                    hostIP = "N/A";
                if (String.IsNullOrEmpty(hostName))
                    hostName = "N/A";
            }

            try
            {
                sEventMessage += String.Format("URL: {0}\n", HttpContext.Request.Url);
                sEventMessage += String.Format("User agent: {0}\n", HttpContext.Request.UserAgent);
                sEventMessage += String.Format("Client host name: {0}\n", hostName);
                sEventMessage += String.Format("Client host IP: {0}\n", hostIP);
                sEventMessage += String.Format("Web server DateTime: {0}\n", DateTime.Now);
                sEventMessage += String.Format("User Login: {0}\n", userLogin);
            }
            catch
            {
            }

            sEventMessage += "\n";
            sEventMessage += GetFullErrorText(exception);

            return sEventMessage.Trim();
        }

        private static string GetFullErrorText(Exception p_Exception)
        {
            string sEventMessage = string.Empty;
            if (p_Exception != null)
            {
                bool bIsInnerException = false;
                while (p_Exception != null)
                {
                    if (!bIsInnerException)
                    {
                        sEventMessage += "An unhandled exception has occurred:";
                    }
                    else
                        sEventMessage += "Inner exception:";
                    sEventMessage += String.Format("\r\nMessage: {0}\r\n\r\nStack trace:\r\n{1}\r\n\r\n\r\n", p_Exception.Message, p_Exception.StackTrace);
                    p_Exception = p_Exception.InnerException;
                    bIsInnerException = true;
                }
            }
            else
                sEventMessage += "Unknown error - no source found.";

            return sEventMessage;
        }

        private string GetRequestedUrl(HttpRequestBase request)
        {
            return String.Equals(request.AppRelativeCurrentExecutionFilePath, "~/errors/notfound", StringComparison.OrdinalIgnoreCase)
                       ? ExtractOriginalUrlFromExecuteUrlModeErrorRequest(request.Url)
                       : request.Url.OriginalString;
        }

        private string GetReferrerUrl(HttpRequestBase request, string url)
        {
            return request.UrlReferrer != null && request.UrlReferrer.OriginalString != url
                       ? request.UrlReferrer.OriginalString
                       : null;
        }

        /// <summary>
        /// Handles the case when a web.config &lt;error statusCode="404" path="/notfound" responseMode="ExecuteURL" /&gt; is triggered.
        /// The original URL is passed via the querystring.
        /// </summary>
        private string ExtractOriginalUrlFromExecuteUrlModeErrorRequest(Uri url)
        {
            // Expected format is "?404;http://hostname.com/some/path"
            var start = url.Query.IndexOf(';');
            if (0 <= start && start < url.Query.Length - 1)
            {
                return url.Query.Substring(start + 1);
            }
            else
            {
                // Unexpected format, so just return the full URL!
                return url.ToString();
            }
        }
    }
}