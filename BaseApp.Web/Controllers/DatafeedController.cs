﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using BaseApp.Web.Code.Infrastructure.BaseControllers;
using BaseApp.Web.Models.Datafeed;

namespace BaseApp.Web.Controllers
{
    [SessionState(SessionStateBehavior.Disabled)]
    public class DatafeedController : ControllerBaseNoAuthorize
    {
        [HttpPost]
        public ActionResult GetUsers(string prefixText, int count, string contextKey)
        {
            return AutocompleteResult(UnitOfWork.Users.GetUsersByFilter(prefixText, count).ConvertAll(m => "m.FullName").ToArray());
        }

        /// <summary>
        /// return Label: "[No matches found]" if input is empty
        /// </summary>
        private ActionResult AutocompleteResult(string[] input)
        {
            var res = new List<AutocompleteItemModel>();

            if (input == null || input.Length < 1)
            {
                res.Add(new AutocompleteItemModel() {Label = "[No matches found]"});
            }
            else
            {
                res.AddRange(input.Select(m => new AutocompleteItemModel() { Label = m, Value = m }));
            }
            return Json(res);
        }
    }
}
