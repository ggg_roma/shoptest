﻿using System.Web.Mvc;
using BaseApp.Web.Code.Infrastructure.BaseControllers;

namespace BaseApp.Web.Controllers
{
    public class HomeController : ControllerBaseNoAuthorize
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "ShopTest");
        }
    }
}
