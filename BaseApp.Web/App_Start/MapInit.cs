﻿using System;
using System.Reflection;
using BaseApp.Web.Code.Mappers;
using AutoMapper;
using System.Linq;

namespace BaseApp.Web.App_Start
{
    public static class MapInit
    {
        public static void Init()
        {
            var profileType = typeof(MapperBase);
            // Get an instance of each MapperBase in the executing assembly.
            var profiles = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => profileType.IsAssignableFrom(t)
                    && t.GetConstructor(Type.EmptyTypes) != null)
                .Select(Activator.CreateInstance)
                .Cast<MapperBase>()
                .ToList();

            // Initialize AutoMapper with each instance of the profiles found.
            Mapper.Initialize(a => profiles.ForEach(a.AddProfile));

            Mapper.AssertConfigurationIsValid();
        }
    }
}