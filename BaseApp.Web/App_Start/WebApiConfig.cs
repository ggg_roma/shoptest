﻿using System.Web.Http;
using BaseApp.Common;
using BaseApp.Web.Code.Filters;
using BaseApp.Web.Code.Formatters;

namespace BaseApp.Web.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Filters.Add(new ApiExceptionFilter());
            config.Filters.Add(new ApiInvalidModelStateFilterAttribute());

            if (ConfigSettings.IsTraceEnabled)
            {
                config.MessageHandlers.Add(new ApiUsageDelegatingHandler());
            }

            config.Formatters.XmlFormatter.UseXmlSerializer = true;

            //http://www.west-wind.com/weblog/posts/2012/Apr/02/Creating-a-JSONP-Formatter-for-ASPNET-Web-API
            GlobalConfiguration.Configuration.Formatters.Insert(0, new JsonpMediaTypeFormatter());

            GlobalConfiguration.Configuration.Formatters.Add(new MultipartDataMediaFormatter.FormMultipartEncodedMediaTypeFormatter());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
