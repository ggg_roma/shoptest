using System.Configuration;
using System.Web.Optimization;
using BaseApp.Web.Code.Infrastructure.Bundling;

namespace BaseApp.Web.App_Start
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            AddJS(bundles);
            AddCSS(bundles);

            //http://www.asp.net/mvc/tutorials/mvc-4/bundling-and-minification
            BundleTable.EnableOptimizations = bool.Parse(ConfigurationManager.AppSettings["BundleEnableOptimizations"]);
        }

        private static void AddJS(BundleCollection bundles)
        {
            bool javaScriptCompressEnabled = bool.Parse(ConfigurationManager.AppSettings["JavascriptCompressEnabled"]);

            var jsMinify =
                new JavascriptTransform(new JavascriptTransformOptions() { EnableMinification = javaScriptCompressEnabled });
            var b = CreateBundle("~/scripts/virtual.js", jsMinify);
            b.Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/jquery.json-{version}.js",
                "~/Scripts/jquery.cookie.js",
                "~/Scripts/base64.js",
                "~/Scripts/growl.js",
                

                //used for client input validation
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Scripts/jquery.validate.unobtrusive.bootstrap.custom.js",

                //used for AjaxHelper's methods (Ajax.BeginForm, etc...)
                "~/Scripts/jquery.unobtrusive-ajax.js",

                "~/Scripts/underscore.js",

                // for PagingSorting
                "~/Scripts/SerializeObject.js",
                "~/Scripts/Common/FilterHighlight.js",
                "~/Scripts/Common/PagingSorting.js",

                //custom scripts
                "~/Scripts/Common/popup.js",
                "~/Scripts/Common/SiteScriptMessage.js",
                "~/Scripts/Common/SiteAutocomplete.js",
                "~/Scripts/Common/SiteValidators.js",
                "~/Scripts/Common/SiteAjaxSetup.js",
                "~/Scripts/Common/Site.js",

                //place after site.js (after templates init)
                "~/Scripts/Common/SiteDialog.js"
                );
            bundles.Add(b);
        }

        private static void AddCSS(BundleCollection bundles)
        {
            bool cssCompressEnabled = bool.Parse(ConfigurationManager.AppSettings["CSSCompressEnabled"]);

            var cssMinify = new CssTransform(new CssTransformOptions() {EnableMinification = cssCompressEnabled});
            var b = CreateBundle("~/content/themes/base/virtual.css", cssMinify);
            b.Include("~/Content/themes/base/all.css");
            bundles.Add(b);

            b = CreateBundle("~/content/growl/virtual.css", cssMinify);
            b.Include("~/Content/growl/growl.css");
            bundles.Add(b);

            b = CreateBundle("~/content/virtual.css", cssMinify);
            b.Include(
                //"~/Content/font-icons/entypo/css/animation.css",
                //"~/Content/font-icons/entypo/css/entypo.css",

                "~/Content/bootstrap.css",
                "~/Content/bootstrap-theme.css",
                "~/Content/bootstrap-override.css",

                "~/Content/Site.css");
            bundles.Add(b);
        }

        private static Bundle CreateBundle(string virtualPath, IBundleTransform transform)
        {
            var bundle = new Bundle(virtualPath);
            bundle.Transforms.Add(transform);
            //used for avoid file ordering by Bundle (order as added from BundleConfig)
            bundle.Orderer = new NullOrderer();
            return bundle;
        }
    }
}