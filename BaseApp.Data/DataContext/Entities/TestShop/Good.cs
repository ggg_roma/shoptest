﻿using System.Collections.Generic;

namespace BaseApp.Data.DataContext.Entities.TestShop
{
    public class Good
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<OrderGood> OrderGoods { get; set; }
    }
}