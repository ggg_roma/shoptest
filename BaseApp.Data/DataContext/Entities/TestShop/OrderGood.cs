﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BaseApp.Data.DataContext.Entities.TestShop
{
    public class OrderGood
    {
        public int Id { get; set; }
        
        [ForeignKey(nameof(Order))]
        public int OrderId { get; set; }

        [ForeignKey(nameof(Good))]
        public int GoodId { get; set; }
       
        public virtual Order Order { get; set; }

        public virtual Good Good { get; set; }
    }
}