﻿using System.Collections.Generic;

namespace BaseApp.Data.DataContext.Entities.TestShop
{
    public class TestUser
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}