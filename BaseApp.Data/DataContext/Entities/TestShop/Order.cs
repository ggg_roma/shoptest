﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BaseApp.Data.DataContext.Entities.TestShop
{
    public class Order
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [ForeignKey(nameof(User))]
        public int UserId { get; set; }

        public virtual TestUser User { get; set; }

        public virtual ICollection<OrderGood> OrderGoods { get; set; }
    }
}