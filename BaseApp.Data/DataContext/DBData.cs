﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Reflection;
using BaseApp.Data.DataContext.Entities;
using BaseApp.Data.DataContext.Entities.TestShop;

namespace BaseApp.Data.DataContext
{
    public class DBData : DbContext
    {
        private static Type _sqlProviderType;
        static DBData()
        {
            // Without this line, EF6 will break.
            _sqlProviderType = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
            
        }

        public static void SetInitializer(IDatabaseInitializer<DBData> strategy)
        {
            Database.SetInitializer(strategy);
        }

        public static DBData Create(bool needTrace)
        {
            var dbData = new DBData();
            if (needTrace)
            {
                dbData.Database.Log = str => Trace.WriteLine(str);
            }
            return dbData;
        }

        public DBData(): base("name=DBData")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ForeignKeyIndexConvention>();

            modelBuilder.Properties<string>().Configure(c => c.HasMaxLength(512));
            modelBuilder.Properties<string>().Where(x =>
                {
                    var lowered = x.Name.ToLower();
                    return lowered.Contains("description") || lowered.Contains("html");
                }).Configure(c=>c.IsMaxLength());

            modelBuilder.Configurations.Add(new UserConfiguration());
        }
        
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Attachment> Attachments { get; set; }
        public virtual DbSet<UserForgotPassword> UserForgotPasswords { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<State> States { get; set; }

        public virtual DbSet<TestUser> TestUsers { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Good> Goods { get; set; }
        public virtual DbSet<OrderGood> OrderGoods { get; set; }
    }
}
