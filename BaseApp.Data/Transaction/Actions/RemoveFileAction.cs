﻿using System;
using System.IO;
using BaseApp.Common;

namespace BaseApp.Data.Transaction.Actions
{
    public class RemoveFileAction : ActionBase
    {
        private readonly string FilePath;

        public RemoveFileAction(string filePath)
        {
            FilePath = filePath;
        }

        public override void Execute()
        {
            try
            {
                File.Delete(FilePath);
            }
            catch (Exception ex)
            {
                LogHolder.MainLog.ErrorException("RemoveFileAction error remove file - " + FilePath, ex);
            }
        }
    }
}
