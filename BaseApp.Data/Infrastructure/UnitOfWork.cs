﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using BaseApp.Common;
using BaseApp.Data.DataContext;
using BaseApp.Data.DataRepository;
using BaseApp.Data.Exceptions;
using BaseApp.Data.Transaction;

namespace BaseApp.Data.Infrastructure
{
    public class UnitOfWork : IDisposable
    {
        private readonly DBData Context;
        private readonly DataContextProvider ContextProvider;

        private DbContextTransactionWrapper CurrentTransaction { get; set; }

        public UnitOfWork()
        {
            Context = DBData.Create(ConfigSettings.IsTraceEnabled);
            ContextProvider = new DataContextProvider(Context);
        }

        #region repositories

        public UserRepository Users => GetRepository<UserRepository>();
        public CountryRepository Countries => GetRepository<CountryRepository>();

        public TestUserRepository TestUsers => GetRepository<TestUserRepository>();
        public GoodRepository Goods => GetRepository<GoodRepository>();
        public OrderRepository Orders => GetRepository<OrderRepository>();
        public OrderGoodRepository OrderGoods => GetRepository<OrderGoodRepository>();

        #endregion

        public void SaveChanges()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                //used to extend Message property with validation errors info
                throw new DbEntityValidationExceptionWrapper(ex);
            }
            catch (DbUpdateException ex)
            {
                //used to analyze exception reason
                throw new DbUpdateExceptionWrapper(ex);
            }
        }

        public void TestConnection()
        {
            var test = Context.Database.SqlQuery<int>(" select 1 ").FirstOrDefault();
        }

        /// <summary>
        /// start new transaction or return NestedTransactionWrapper for already started transaction
        /// </summary>
        public ITransactionWrapper BeginTransaction()
        {
            if (CurrentTransaction != null && !CurrentTransaction.IsDisposed)
            {
                return new NestedTransactionWrapper(CurrentTransaction);
            }
            else
            {
                CurrentTransaction = new DbContextTransactionWrapper(Context);
                return CurrentTransaction;
            }
        }

        private T GetRepository<T>() where T : RepositoryBase
        {
            return ContextProvider.GetRepository<T>();
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
