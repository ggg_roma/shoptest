﻿using System.Data.Entity;

namespace BaseApp.Data.Infrastructure
{
    public class RepositoryBase
    {
        protected DataContextProvider Context { get; set; }

        protected RepositoryBase(DataContextProvider context)
        {
            Context = context;
        }

        protected virtual T CreateEmpty<T>(bool addToContext = true) where T : class
        {
            var entity = Context.Set<T>().Create();
            if (addToContext)
                Context.Set<T>().Add(entity);
            return entity;
        }

        protected void MarkForSave<T>(T entity) where T : class
        {
            if (Context.IsNewInstance(entity))
            {
                MarkForInsert(entity);
            }
            else
            {
                MarkForUpdate(entity);
            }
        }

        protected void MarkForInsert<T>(T entity) where T : class
        {
            Context.Entry(entity).State = EntityState.Added;
        }

        protected void MarkForUpdate<T>(T entity) where T : class
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        protected void MarkForDelete<T>(T entity) where T : class
        {
            Context.Entry(entity).State = EntityState.Deleted;
        }

        protected T GetRepository<T>() where T : RepositoryBase
        {
            return Context.GetRepository<T>();
        }
    }
}
