﻿namespace BaseApp.Data.Infrastructure
{
    public class UnitOfWorkFactory : IUnitOfWorkFactoryPerCall, IUnitOfWorkFactoryPerRequest
    {
        public UnitOfWorkFactory()
        {
            UnitOfWork = new UnitOfWork();
        }

        public UnitOfWork UnitOfWork
        {
            get; private set;
        }

        public void Dispose()
        {
            UnitOfWork.Dispose();
        }
    }
}
