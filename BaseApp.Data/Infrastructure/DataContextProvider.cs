﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using BaseApp.Data.DataContext;

namespace BaseApp.Data.Infrastructure
{
    /// <summary>
    /// used as DataContext wrapper to avoid DataContext.SaveChanges method call from repositories
    /// </summary>
    public class DataContextProvider
    {
        private DBData Context { get; set; }
        public StoredProcedureProvider StoredProcedures { get; private set; }
        private readonly Dictionary<Type, RepositoryBase> Repositories;

        public DataContextProvider(DBData context)
        {
            Context = context;
            StoredProcedures = new StoredProcedureProvider(context);
            Repositories = new Dictionary<Type, RepositoryBase>();
        }

        public DbSet<T> Set<T>() where T : class
        {
            return Context.Set<T>();
        }

        public DbEntityEntry Entry(object entity)
        {
            return Context.Entry(entity);
        }

        public DbEntityEntry<T> Entry<T>(T entity) where T : class
        {
            return Context.Entry(entity);
        }

        public bool IsNewInstance<T>(T entity) where T : class
        {
            DbSet<T> l_EntitySet = Context.Set<T>();
            var objContext = ((IObjectContextAdapter)Context).ObjectContext;
            ObjectSet<T> objectSet = objContext.CreateObjectSet<T>();
            var key = objContext.CreateEntityKey(string.Format("{0}.{1}", objectSet.EntitySet.EntityContainer.Name, objectSet.EntitySet.Name), entity);

            if (key.EntityKeyValues.Length == 0)
            {
                throw new Exception("No entity key info found - " + typeof(T).Name);
            }

            bool isNew = false;
            if (key.EntityKeyValues.Length == 1)
            {
                string sKey = key.EntityKeyValues.First().Value.ToString();
                int iKey;
                if (int.TryParse(sKey, out iKey) && iKey == 0)
                {
                    isNew = true;
                }
            }

            if (!isNew)
            {
                T find = l_EntitySet.Find(key.EntityKeyValues.Select(c => c.Value).ToArray());
                if (find != null)
                {
                    if (!ReferenceEquals(find, entity))
                    {
                        Context.Entry(find).State = EntityState.Detached;
                    }
                }
                else
                {
                    isNew = true;
                }
            }

            return isNew;
        }

        public List<T> ExecStoredProcedure<T>(string storedProcedureName, Dictionary<string, object> parameters = null)
        {
            var spBuilder = new StringBuilder();
            spBuilder.AppendFormat("{0}", storedProcedureName);
            
            var sqlParameters = new List<SqlParameter>();
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    var parameterName = parameter.Key;
                    if (!parameterName.StartsWith("@"))
                    {
                        parameterName = "@" + parameterName;
                    }
                    spBuilder.AppendFormat(" {0}", parameterName);;
                    sqlParameters.Add(new SqlParameter(parameterName, parameter.Value ?? DBNull.Value));
                }
            }

            return Context.Database.SqlQuery<T>(spBuilder.ToString(), sqlParameters.ToArray()).ToList<T>();
        }

        public DataSet ExecStoredProcedure(string storedProcedureName, Dictionary<string, object> parameters = null)
        {
            var conn = (SqlConnection)Context.Database.Connection;
            var cmd = new SqlCommand(storedProcedureName, conn)
            {
                CommandType = CommandType.StoredProcedure
            };

            if (parameters != null)
            {
                foreach (KeyValuePair<string, object> parameterVal in parameters)
                {
                    string parameterName = parameterVal.Key;
                    if (!parameterName.StartsWith("@"))
                    {
                        parameterName = "@" + parameterName;
                    }
                    cmd.Parameters.AddWithValue(parameterName, (parameterVal.Value ?? DBNull.Value));
                }
            }
            
            var dataSet = new DataSet();
            using (var adapter = new SqlDataAdapter(cmd))
            {
                adapter.Fill(dataSet);
            }

            return dataSet;
        }

        public IEnumerable<string> GetEntityKeys<T>() where T : class
        {
            ObjectContext objectContext = ((IObjectContextAdapter)Context).ObjectContext;
            ObjectSet<T> set = objectContext.CreateObjectSet<T>();
            return set.EntitySet.ElementType.KeyMembers.Select(k => k.Name);
        }

        internal T GetRepository<T>() where T : RepositoryBase
        {
            Type type = typeof(T);

            RepositoryBase repository;
            if (!Repositories.TryGetValue(type, out repository))
            {
                repository = (RepositoryBase)Activator.CreateInstance(type, this);
                Repositories.Add(type, repository);
            }

            return (T)repository;
        }
    }
}
