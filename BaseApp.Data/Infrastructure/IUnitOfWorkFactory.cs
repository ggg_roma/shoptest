﻿using System;

namespace BaseApp.Data.Infrastructure
{
    public interface IUnitOfWorkFactoryPerCall : IUnitOfWorkFactory { }
    public interface IUnitOfWorkFactoryPerRequest : IUnitOfWorkFactory { }

    public interface IUnitOfWorkFactory : IDisposable
    {
        UnitOfWork UnitOfWork { get; }
    }
}
