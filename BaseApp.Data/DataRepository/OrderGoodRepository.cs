﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using BaseApp.Data.DataContext.Entities.TestShop;
using BaseApp.Data.Infrastructure;

namespace BaseApp.Data.DataRepository
{
    public class OrderGoodRepository : RepositoryEntityBase<OrderGood>
    {
        public OrderGoodRepository(DataContextProvider context)
            : base(context)
        {
        }

        public async Task<List<Good>> GetGoodsByOrderIdAsync(int orderId)
        {
            return await EntitySet.Where(x => x.OrderId == orderId).Select(x => x.Good).OrderBy(x => x.Name).ToListAsync();
        }
    }
}