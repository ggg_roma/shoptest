﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseApp.Data.DataContext;
using BaseApp.Data.Infrastructure.Data;

namespace BaseApp.Data.DataRepository
{
    public class NotificationEmailRepository : RepositoryEntityBase<NotificationEmail>
    {
        public NotificationEmailRepository(DataContextProvider context)
            : base(context)
        {
        }
    }
}
