﻿using BaseApp.Data.DataContext.Entities.TestShop;
using BaseApp.Data.Infrastructure;

namespace BaseApp.Data.DataRepository
{
    public class GoodRepository : RepositoryEntityBase<Good>
    {
        public GoodRepository(DataContextProvider context)
            : base(context)
        {
        }
    }
}