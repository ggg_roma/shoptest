﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using BaseApp.Data.DataContext.Entities.TestShop;
using BaseApp.Data.Infrastructure;

namespace BaseApp.Data.DataRepository
{
    public class OrderRepository : RepositoryEntityBase<Order>
    {
        public OrderRepository(DataContextProvider context)
            : base(context)
        {
        }

        public async Task<List<Order>> GetOrdersByUserIdAsync(int userId)
        {
            return await EntitySet.Where(x => x.UserId == userId).OrderBy(x => x.Name).ToListAsync();
        }
    }
}