﻿using System;
using System.Collections.Generic;
using System.Linq;
using BaseApp.Common;
using System.Data.Entity;
using BaseApp.Data.DataContext.Entities;
using BaseApp.Data.DataContext.Projections.Users;
using BaseApp.Data.Extensions;
using BaseApp.Data.Infrastructure;

namespace BaseApp.Data.DataRepository
{
    public class UserRepository : RepositoryEntityDeletableBase<User>
    {
        public UserRepository(DataContextProvider context)
            : base(context)
        {
        }

        public List<User> GetDeleted()
        {
            return EntitySet.Where(m => m.DeletedDate != null).ToList();
        }

        public List<User> GetUsersByFilter(string prefix, int count)
        {
            if (String.IsNullOrWhiteSpace(prefix))
                prefix = null;
            return EntitySetNotDeleted
                .Where(m => (prefix == null || m.FirstName.StartsWith(prefix) || m.LastName.StartsWith(prefix)))
                .Take(count)
                .ToList();
        }

        public User GetIncludeRolesOrNull(int id)
        {
            return EntitySet
                .Include(user => user.Roles)
                .FirstOrDefault(m => m.Id == id);
        }

        public User GetByEmailOrNull(string email, bool includeDeleted = false)
        {
            return GetUserView(includeDeleted)
                .Include(user => user.Roles)
                .FirstOrDefault(m => m.Email == email);
        }

        public User GetByLoginOrNull(string login, bool includeDeleted = false)
        {
            return GetUserView(includeDeleted)
                .Include(user => user.Roles)
                .FirstOrDefault(m => m.Login == login);
        }

        public AccountProjection GetAccountByLoginOrNull(string login)
        {
            return EntitySetNotDeleted
                .Where(m => m.Login == login)
                .Select(m => new AccountProjection
                             {
                                 Id = m.Id,
                                 Login = m.Login,
                                 Password = m.Password,
                                 FirstName = m.FirstName,
                                 LastName = m.LastName,
                                 Email = m.Email,
                                 Roles = m.Roles.Select(t => t.Name)
                             })
                .FirstOrDefault();
        }

        private IQueryable<User> GetUserView(bool includeDeleted = false)
        {
            var q = EntitySet.AsQueryable();
            if (!includeDeleted)
            {
                q = q.GetNotDeleted();
            }
            return q;
        }

        public List<Role> GetAllRoles()
        {
            return Context.Set<Role>().ToList();
        }

        public Role GetUserRole()
        {
            return Context.Set<Role>().Single(m => m.Name == Constants.Roles.User);
        }

        public Role GetAdminRole()
        {
            return Context.Set<Role>().Single(m => m.Name == Constants.Roles.Admin);
        }

        public UserForgotPassword GetForgotPasswordRequest(Guid id)
        {
            return Context.Set<UserForgotPassword>().SingleOrDefault(m => m.RequestGuid == id);
        }
    }
}
