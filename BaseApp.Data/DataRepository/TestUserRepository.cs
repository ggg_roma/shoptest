﻿using BaseApp.Data.DataContext.Entities.TestShop;
using BaseApp.Data.Infrastructure;

namespace BaseApp.Data.DataRepository
{
    public class TestUserRepository : RepositoryEntityBase<TestUser>
    {
        public TestUserRepository(DataContextProvider context)
            : base(context)
        {
        }
    }
}