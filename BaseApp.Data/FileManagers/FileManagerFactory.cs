﻿using BaseApp.Common;

namespace BaseApp.Data.FileManagers
{
    public static class FileManagerFactory
    {
        private static FileManager _Attachments { get; set; }

        static FileManagerFactory()
        {
            _Attachments = new FileManager(ConfigSettings.AttachmentsFolder);
        }

        public static FileManager Attachments
        {
            get { return _Attachments; }
        }
    }
}
