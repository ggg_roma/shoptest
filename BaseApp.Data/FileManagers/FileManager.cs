﻿using System;
using System.IO;
using BaseApp.Data.Transaction;
using BaseApp.Data.Transaction.Actions;

namespace BaseApp.Data.FileManagers
{
    public class FileManager
    {
        private string DirectoryPath { get; set; }

        public FileManager(string directoryPath)
        {
            DirectoryPath = directoryPath;

            if (!Directory.Exists(DirectoryPath))
            {
                Directory.CreateDirectory(DirectoryPath);
            }
        }

        /// <summary>
        /// save file and return generated unique file name
        /// </summary>
        public string SaveFileWithUniqueName(string fileName, byte[] content, ITransactionWrapper tran)
        {
            string genFileName = GenerateUniqueFileName(fileName);
            SaveFile(genFileName, content, tran);

            return genFileName;
        }

        public void SaveFile(string fileName, byte[] content, ITransactionWrapper tran)
        {
            string path = GetFilePath(fileName);
            File.WriteAllBytes(path, content);

            tran.RegisterAfterRollbackAction(new RemoveFileAction(path));
        }

        public byte[] GetFileContent(string fileName)
        {
            return File.ReadAllBytes(GetFilePath(fileName));
        }

        public void DeleteFile(string fileName, ITransactionWrapper tran)
        {
            tran.RegisterAfterCommitAction(new RemoveFileAction(GetFilePath(fileName)));
        }

        public string GenerateUniqueFileName(string fileName)
        {
            return String.Format("{0}_{1:N}{2}",
                Path.GetFileNameWithoutExtension(fileName),
                Guid.NewGuid(),
                Path.GetExtension(fileName));
        }

        public string RecoverOriginalFileName(string generatedUniqueFileName)
        {
            if (String.IsNullOrWhiteSpace(generatedUniqueFileName))
                return "";

            int index = generatedUniqueFileName.LastIndexOf("_", StringComparison.InvariantCulture);
            string originalFileName = generatedUniqueFileName.Substring(0, index) + Path.GetExtension(generatedUniqueFileName);

            return originalFileName;
        }

        private string GetFilePath(string fileName)
        {
            return Path.Combine(DirectoryPath, fileName);
        }
    }
}
