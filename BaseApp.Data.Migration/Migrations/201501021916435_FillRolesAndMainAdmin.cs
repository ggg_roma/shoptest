using System.Data.Entity.Migrations;
using BaseApp.Common;
using BaseApp.Common.Utils;

namespace BaseApp.Data.Migration.Migrations
{
    public partial class FillRolesAndMainAdmin : DbMigration
    {
        public override void Up()
        {
            Sql(string.Format(RolesInsertSqlTemplate, 1, Constants.Roles.Admin));
            Sql(string.Format(RolesInsertSqlTemplate, 2, Constants.Roles.User));
            Sql(string.Format(AdminUserInsertSqlTemplate, PasswordHash.HashPassword("1111111")));
        }
        
        public override void Down()
        {
            Sql(" delete from dbo.[UserRole] where UserId in (select U.Id from dbo.[User] U where U.[Login] = 'admin')  ");
            Sql(" delete from dbo.[User] where [Login] = 'admin' ");
            Sql(" DELETE FROM dbo.[Role]" );
        }

        private const string RolesInsertSqlTemplate = @" insert into dbo.[Role] (Id, [Name] ) values ({0}, '{1}') ";

        private const string AdminUserInsertSqlTemplate = @" 
insert into dbo.[User] ([Login], [Password], FirstName, LastName, Email, CreatedDate, UpdatedDate)
values ('admin', '{0}', 'Admin', 'User', 'admin@example.com', GETDATE(), GETDATE())

insert into UserRole (UserId, RoleId)
values (SCOPE_IDENTITY(), 1)";
    }
}
