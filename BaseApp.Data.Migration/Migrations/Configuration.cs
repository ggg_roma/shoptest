using System.Data.Entity.Migrations;
using System.Linq;
using BaseApp.Data.DataContext.Entities;
using BaseApp.Data.DataContext.Entities.TestShop;

namespace BaseApp.Data.Migration.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<BaseApp.Data.DataContext.DBData>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Data.DataContext.DBData context)
        {
            base.Seed(context);

            context.TestUsers.AddOrUpdate(
                new TestUser {Id = 1, Name = "User1"},
                new TestUser {Id = 2, Name = "User2"},
                new TestUser {Id = 3, Name = "User3"});
              
            context.Goods.AddOrUpdate(
                new Good { Id = 1, Name = "Good1" }, 
                new Good { Id = 2, Name = "Good2" }, 
                new Good { Id = 3, Name = "Good3" },
                new Good { Id = 4, Name = "Good4" }, 
                new Good { Id = 5, Name = "Good5" }, 
                new Good { Id = 6, Name = "Good6" }, 
                new Good { Id = 7, Name = "Good7" },
                new Good { Id = 8, Name = "Good8" }, 
                new Good { Id = 9, Name = "Good9" }, 
                new Good { Id = 10, Name = "Good10" },
                new Good { Id = 11, Name = "Good11" },
                new Good { Id = 12, Name = "Good12" },
                new Good { Id = 13, Name = "Good13" },
                new Good { Id = 14, Name = "Good14" },
                new Good { Id = 15, Name = "Good15" },
                new Good { Id = 16, Name = "Good16" },
                new Good { Id = 17, Name = "Good17" },
                new Good { Id = 18, Name = "Good18" },
                new Good { Id = 19, Name = "Good19" },
                new Good { Id = 20, Name = "Good20" });

            context.Orders.AddOrUpdate(
                new Order {Id = 1, Name = "Order1", UserId = 1},
                new Order {Id = 2, Name = "Order2", UserId = 1 },
                new Order {Id = 3, Name = "Order3", UserId = 1 },
                new Order {Id = 4, Name = "Order4", UserId = 2 },
                new Order {Id = 5, Name = "Order5", UserId = 2 },
                new Order {Id = 6, Name = "Order6", UserId = 3 });
              
            //Order1
            context.OrderGoods.AddOrUpdate(
                new OrderGood { Id = 1, OrderId = 1, GoodId = 1}, 
                new OrderGood { Id = 2, OrderId = 1, GoodId = 2 }, 
                new OrderGood { Id = 3, OrderId = 1, GoodId = 3 },
                new OrderGood { Id = 4, OrderId = 1, GoodId = 4 }, 
                new OrderGood { Id = 5, OrderId = 1, GoodId = 5 }, 
                new OrderGood { Id = 6, OrderId = 1, GoodId = 6 }, 
                new OrderGood { Id = 7, OrderId = 1, GoodId = 7 },
                new OrderGood { Id = 8, OrderId = 1, GoodId = 8 }, 
                new OrderGood { Id = 9, OrderId = 1, GoodId = 9 },
                new OrderGood { Id = 10, OrderId = 1, GoodId = 10 },
                new OrderGood { Id = 54, OrderId = 1, GoodId = 11 },
                new OrderGood { Id = 55, OrderId = 1, GoodId = 12 },
                new OrderGood { Id = 56, OrderId = 1, GoodId = 13 },
                new OrderGood { Id = 57, OrderId = 1, GoodId = 14 },
                new OrderGood { Id = 58, OrderId = 1, GoodId = 15 },
                new OrderGood { Id = 59, OrderId = 1, GoodId = 16 },
                new OrderGood { Id = 60, OrderId = 1, GoodId = 17 },
                new OrderGood { Id = 61, OrderId = 1, GoodId = 18 },
                new OrderGood { Id = 62, OrderId = 1, GoodId = 19 },
                new OrderGood { Id = 63, OrderId = 1, GoodId = 20 });
            //Order2
            context.OrderGoods.AddOrUpdate(
                new OrderGood { Id = 11, OrderId = 2, GoodId = 12 }, 
                new OrderGood { Id = 12, OrderId = 2, GoodId = 10 },
                new OrderGood { Id = 13, OrderId = 2, GoodId = 11 },
                new OrderGood { Id = 14, OrderId = 2, GoodId = 8 }, 
                new OrderGood { Id = 15, OrderId = 2, GoodId = 5 }, 
                new OrderGood { Id = 16, OrderId = 2, GoodId = 6 },
                new OrderGood { Id = 17, OrderId = 2, GoodId = 7 }, 
                new OrderGood { Id = 18, OrderId = 2, GoodId = 1 }, 
                new OrderGood { Id = 19, OrderId = 2, GoodId = 3 });
            //Order3
            context.OrderGoods.AddOrUpdate(
                new OrderGood { Id = 20, OrderId = 3, GoodId = 8},
                new OrderGood { Id = 21, OrderId = 3, GoodId = 1}, 
                new OrderGood { Id = 22, OrderId = 3, GoodId = 5},
                new OrderGood { Id = 23, OrderId = 3, GoodId = 6}, 
                new OrderGood { Id = 24, OrderId = 3, GoodId = 2},
                new OrderGood { Id = 25, OrderId = 3, GoodId = 7});
            //Order4
            context.OrderGoods.AddOrUpdate(
                new OrderGood { Id = 26, OrderId = 4, GoodId = 2 },
                new OrderGood { Id = 27, OrderId = 4, GoodId = 4 },
                new OrderGood { Id = 28, OrderId = 4, GoodId = 6 },
                new OrderGood { Id = 29, OrderId = 4, GoodId = 10 },
                new OrderGood { Id = 30, OrderId = 4, GoodId = 12 },
                new OrderGood { Id = 31, OrderId = 4, GoodId = 1 });
            //Order5
            context.OrderGoods.AddOrUpdate(
                new OrderGood { Id = 32, OrderId = 5, GoodId = 2 },
                new OrderGood { Id = 33, OrderId = 5, GoodId = 4 },
                new OrderGood { Id = 34, OrderId = 5, GoodId = 6 },
                new OrderGood { Id = 35, OrderId = 5, GoodId = 8 },
                new OrderGood { Id = 36, OrderId = 5, GoodId = 10 },
                new OrderGood { Id = 37, OrderId = 5, GoodId = 12 },
                new OrderGood { Id = 38, OrderId = 5, GoodId = 1 },
                new OrderGood { Id = 39, OrderId = 5, GoodId = 3 },
                new OrderGood { Id = 40, OrderId = 5, GoodId = 5 },
                new OrderGood { Id = 41, OrderId = 5, GoodId = 7 },
                new OrderGood { Id = 42, OrderId = 5, GoodId = 9 });
            //Order6
            context.OrderGoods.AddOrUpdate(
                new OrderGood { Id = 43, OrderId = 6, GoodId = 12 },
                new OrderGood { Id = 44, OrderId = 6, GoodId = 11 },
                new OrderGood { Id = 45, OrderId = 6, GoodId = 10 },
                new OrderGood { Id = 46, OrderId = 6, GoodId = 9 },
                new OrderGood { Id = 47, OrderId = 6, GoodId = 8 },
                new OrderGood { Id = 48, OrderId = 6, GoodId = 7 },
                new OrderGood { Id = 49, OrderId = 6, GoodId = 6 },
                new OrderGood { Id = 50, OrderId = 6, GoodId = 5 },
                new OrderGood { Id = 51, OrderId = 6, GoodId = 4 },
                new OrderGood { Id = 52, OrderId = 6, GoodId = 3 },
                new OrderGood { Id = 53, OrderId = 6, GoodId = 2 });
        }
    }
}
