namespace BaseApp.Data.Migration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Test_Shop_Tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Good",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderGood",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(nullable: false),
                        GoodId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Good", t => t.GoodId)
                .ForeignKey("dbo.Order", t => t.OrderId);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 512),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TestUser", t => t.UserId);
            
            CreateTable(
                "dbo.TestUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderGood", "OrderId", "dbo.Order");
            DropForeignKey("dbo.Order", "UserId", "dbo.TestUser");
            DropForeignKey("dbo.OrderGood", "GoodId", "dbo.Good");
            DropTable("dbo.TestUser");
            DropTable("dbo.Order");
            DropTable("dbo.OrderGood");
            DropTable("dbo.Good");
        }
    }
}
