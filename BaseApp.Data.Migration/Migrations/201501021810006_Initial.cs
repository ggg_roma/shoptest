using System.Data.Entity.Migrations;

namespace BaseApp.Data.Migration.Migrations
{
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attachment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FileName = c.String(nullable: false, maxLength: 256),
                        GenFileName = c.String(nullable: false, maxLength: 512),
                        FileSize = c.Long(nullable: false),
                        ContentType = c.String(nullable: false, maxLength: 256),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedByUserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.CreatedByUserId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 64),
                        Password = c.String(nullable: false, maxLength: 256),
                        FirstName = c.String(nullable: false, maxLength: 64),
                        LastName = c.String(nullable: false, maxLength: 64),
                        Email = c.String(nullable: false, maxLength: 64),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(nullable: false),
                        UpdatedByUserId = c.Int(),
                        DeletedDate = c.DateTime(),
                        DeletedByUserId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.DeletedByUserId)
                .ForeignKey("dbo.User", t => t.UpdatedByUserId)
                .Index(t => new { t.Login, t.DeletedDate }, unique: true, name: "IX_LoginIndex");
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserForgotPassword",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        RequestGuid = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatorIpAddress = c.String(maxLength: 64),
                        ApprovedDateTime = c.DateTime(),
                        ApproverIpAddress = c.String(maxLength: 64),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.RequestGuid);
            
            CreateTable(
                "dbo.Country",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                        NumericCode = c.Int(),
                        Alpha2 = c.String(nullable: false, maxLength: 2, fixedLength: true, unicode: false),
                        Alpha3 = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        Ordinal = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.State",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        CountryId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 64),
                        Code = c.String(nullable: false, maxLength: 16),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Country", t => t.CountryId);
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.User", t => t.UserId)
                .ForeignKey("dbo.Role", t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.State", "CountryId", "dbo.Country");
            DropForeignKey("dbo.UserForgotPassword", "UserId", "dbo.User");
            DropForeignKey("dbo.User", "UpdatedByUserId", "dbo.User");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.User");
            DropForeignKey("dbo.User", "DeletedByUserId", "dbo.User");
            DropForeignKey("dbo.Attachment", "CreatedByUserId", "dbo.User");
            DropIndex("dbo.UserForgotPassword", new[] { "RequestGuid" });
            DropIndex("dbo.User", "IX_LoginIndex");
            DropTable("dbo.UserRole");
            DropTable("dbo.State");
            DropTable("dbo.Country");
            DropTable("dbo.UserForgotPassword");
            DropTable("dbo.Role");
            DropTable("dbo.User");
            DropTable("dbo.Attachment");
        }
    }
}
