using System.Data.Entity.Migrations;

namespace BaseApp.Data.Migration.Migrations
{
    public partial class AddUserFullName : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE dbo.[User] ADD FullName AS ltrim(rtrim(FirstName + ' ' + LastName))");
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "FullName");
        }
    }
}
