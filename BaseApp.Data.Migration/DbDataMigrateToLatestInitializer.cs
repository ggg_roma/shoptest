﻿using System.Data.Entity;
using BaseApp.Data.DataContext;
using BaseApp.Data.Migration.Migrations;

namespace BaseApp.Data.Migration
{
    public class DbDataMigrateToLatestInitializer : MigrateDatabaseToLatestVersion<DBData, Configuration>
    {
    }
}
